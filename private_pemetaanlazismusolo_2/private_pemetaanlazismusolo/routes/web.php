<?php

use App\Http\Controllers\DDController;
use App\Http\Controllers\DonaturController;
use App\Http\Controllers\DPMController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\HomePMController;
use App\Http\Controllers\JenisDController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\KecamatanController;
use App\Http\Controllers\KelolaAkunController;
use App\Http\Controllers\PMController;
use App\Http\Controllers\ProgramPMController;
use App\Http\Controllers\SubprogramController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\TransaksiPMController;
use App\Models\HomedModel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::middleware('auth')->group(function () {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/donatur-map', [App\Http\Controllers\HomeController::class, 'donatur_map'])->name('donatur-map');

    //map per-kecamatan & jenis
    Route::get('mapkecamatan/{id_kecamatan}', [HomeController::class, 'mapkecamatan']);
    Route::get('mapjenis/{id_jenisdonatur}', [HomeController::class, 'mapjenis']);

    //transaksi per-donatur
    Route::get('rekaptransaksi/{id_dd}', [HomeController::class, 'rekaptransaksi']);
    //transaksi per-pm
    Route::get('rekaptransaksipm/{id_dpm}', [HomePMController::class, 'rekaptransaksipm']);

    //detaildatadontur dan PM
    // Route::get('detaildatadonatur/{id_donatur}', [HomeController::class, 'detaildatadonatur']);
    Route::get('detaildataPM/{id_pm}', [HomePMController::class, 'detaildatapm']);

    //kecamatan
    Route::get('/kecamatan', [App\Http\Controllers\KecamatanController::class, 'index'])->name('kecamatan');
    Route::get('/kecamatan/add', [KecamatanController::class, 'add']);
    Route::post('/kecamatan/insert', [KecamatanController::class, 'insert']);
    Route::get('/kecamatan/edit/{id_kecamatan}', [KecamatanController::class, 'edit']);
    Route::post('/kecamatan/update/{id_kecamatan}', [KecamatanController::class, 'update']);
    Route::get('/kecamatan/delete/{id_kecamatan}', [KecamatanController::class, 'delete']);

    //Jenis Donatur
    Route::get('jenis-donatur', [JenisDController::class, 'index'])->name('jenis-donatur');
    Route::get('jenis-donatur/add', [JenisDController::class, 'add']);
    Route::post('jenis-donatur/insert', [JenisDController::class, 'insert']);
    Route::get('jenis-donatur/edit/{id_jenisdonatur}', [JenisDController::class, 'edit']);
    Route::post('jenis-donatur/update/{id_jenisdonatur}', [JenisDController::class, 'update']);
    Route::get('jenis-donatur/delete/{id_jenisdonatur}', [JenisDController::class, 'delete']);

    //datadonatur
    Route::get('dd', [DDController::class, 'index'])->name('dd');
    Route::get('dd/add', [DDController::class, 'add']);
    Route::post('dd/insert', [DDController::class, 'insert']);
    Route::get('dd/edit/{id_dd}', [DDController::class, 'edit']);
    Route::post('dd/update/{id_dd}', [DDController::class, 'update']);
    Route::get('dd/delete/{id_dd}', [DDController::class, 'delete']);

    //pemetaan donatur
    Route::get('donatur', [DonaturController::class, 'index'])->name('donatur');
    Route::get('donatur/add', [DonaturController::class, 'add']);
    Route::post('donatur/insert', [DonaturController::class, 'insert']);
    Route::get('donatur/edit/{id_donatur}', [DonaturController::class, 'edit']);
    Route::post('donatur/update/{id_donatur}', [DonaturController::class, 'update']);
    Route::get('donatur/delete/{id_donatur}', [DonaturController::class, 'delete']);

    //transaksi donatur
    Route::get('transaksi', [TransaksiController::class, 'index'])->name('transaksi');
    Route::get('transaksi/add', [TransaksiController::class, 'add']);
    Route::post('transaksi/insert', [TransaksiController::class, 'insert']);
    Route::get('transaksi/edit/{id_transaksi}', [TransaksiController::class, 'edit']);
    Route::post('transaksi/update/{id_transaksi}', [TransaksiController::class, 'update']);
    Route::get('transaksi/delete/{id_transaksi}', [TransaksiController::class, 'delete']);

    //map pm
    Route::get('/pm-map', [App\Http\Controllers\HomePMController::class, 'pm_map'])->name('pm-map');

    //map per-kecamatan & program pm
    Route::get('mapkecamatanpm/{id_kecamatan}', [App\Http\Controllers\HomePMController::class, 'mapkecamatanpm']);
    Route::get('mapprogrampm/{id_program}', [App\Http\Controllers\HomePMController::class, 'mapprogrampm']);

    //programpm
    Route::get('program-pm', [ProgramPMController::class, 'index'])->name('program-pm');
    Route::get('program-pm/add', [ProgramPMController::class, 'add']);
    Route::post('program-pm/insert', [ProgramPMController::class, 'insert']);
    Route::get('program-pm/edit/{id_program}', [ProgramPMController::class, 'edit']);
    Route::post('program-pm/update/{id_program}', [ProgramPMController::class, 'update']);
    Route::get('program-pm/delete/{id_program}', [ProgramPMController::class, 'delete']);

    //subprogram
    Route::get('sub-programpm', [SubprogramController::class, 'index'])->name('sub-programpm');
    Route::get('sub-programpm/add', [SubprogramController::class, 'add']);
    Route::post('sub-programpm/insert', [SubprogramController::class, 'insert']);
    Route::get('sub-programpm/edit/{id_subprogram}', [SubprogramController::class, 'edit']);
    Route::post('sub-programpm/update/{id_subprogram}', [SubprogramController::class, 'update']);
    Route::get('sub-programpm/delete/{id_subprogram}', [SubprogramController::class, 'delete']);

    //Data PM
    Route::get('dpm', [DPMController::class, 'index'])->name('dpm');
    Route::get('dpm/add', [DPMController::class, 'add']);
    Route::post('dpm/insert', [DPMController::class, 'insert']);
    Route::get('dpm/edit/{id_dpm}', [DPMController::class, 'edit']);
    Route::post('dpm/update/{id_dpm}', [DPMController::class, 'update']);
    Route::get('dpm/delete/{id_dpm}', [DPMController::class, 'delete']);

    //Pemetaan Penerima Manfaat
    Route::get('penerima-manfaat', [PMController::class, 'index'])->name('penerima-manfaat');
    Route::get('penerima-manfaat/add', [PMController::class, 'add']);
    Route::post('penerima-manfaat/insert', [PMController::class, 'insert']);
    Route::get('penerima-manfaat/edit/{id_pm}', [PMController::class, 'edit']);
    Route::post('penerima-manfaat/update/{id_pm}', [PMController::class, 'update']);
    Route::get('penerima-manfaat/delete/{id_pm}', [PMController::class, 'delete']);
    Route::get('penerima-manfaat/program/', [PMController::class, 'programSearch'])->name('searchProgram');

    //transaksi pm
    Route::get('transaksipm', [TransaksiPMController::class, 'index'])->name('transaksipm');
    Route::get('transaksipm/add', [TransaksiPMController::class, 'add']);
    Route::post('transaksipm/insert', [TransaksiPMController::class, 'insert']);
    Route::get('transaksipm/edit/{id_pm}', [TransaksiPMController::class, 'edit']);
    Route::post('transaksipm/update/{id_pm}', [TransaksiPMController::class, 'update']);
    Route::get('transaksipm/delete/{id_pm}', [TransaksiPMController::class, 'delete']);

    // kelola akun
    Route::get('/kelola-akun', [KelolaAkunController::class, 'index'])->name('kelola-akun');
    Route::get('/akun/add', [KelolaAkunController::class, 'add'])->name('akun-add');
    Route::post('/akun/store', [KelolaAkunController::class, 'store'])->name('akun-store');
    Route::get('/akun-edit/{id}', [KelolaAkunController::class, 'edit'])->name('akun-edit');
    Route::put('/akun-update/{id}', [KelolaAkunController::class, 'update'])->name('akun-update');
    Route::put('/akun-updatepass/{id}', [KelolaAkunController::class, 'update_password'])->name('akun-update-password');
    Route::get('/akun-delete/{id}', [KelolaAkunController::class, 'delete'])->name('akun-delete');
    Route::get('/akun-add', [KelolaAkunController::class, 'add'])->name('akun-add');
});
