<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ProgramPMModel extends Model
{
    public function AllData()
    {
        return DB::table('tbl_program')
            ->get();
    }

    //menangkap data dari fungsi insert
    public function InsertData($data)
    {
        DB::table('tbl_program')
            ->insert($data);
    }

    //ambil data untuk diedit
    public function DetailData($id_program)
    {
        return DB::table('tbl_program')
            ->where('id_program', $id_program)
            ->first(); //berdasarkan idprogram
    }

    public function UpdateData($id_program, $data)
    {
        DB::table('tbl_program')
            ->where('id_program', $id_program)
            ->update($data);
    }

    public function DeleteData($id_program)
    {
        DB::table('tbl_program')
            ->where('id_program', $id_program)
            ->delete();
    }
}
