<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TransaksipmModel extends Model
{
    protected $table = 'tbl_transaksipm';
    public function AllData()
    {
        return DB::table('tbl_transaksipm')
            //fungsi join
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_transaksipm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_transaksipm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_transaksipm.id_subprogram')
            ->get();
    }

    public function InsertData($data)
    {
        DB::table('tbl_transaksipm')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id_transaksipm)
    {
        return DB::table('tbl_transaksipm')
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_transaksipm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_transaksipm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_transaksipm.id_subprogram')
            ->where('id_transaksipm', $id_transaksipm)
            ->first(); //berdasarkan id_transpm
    }

    public function UpdateData($id_transaksipm, $data)
    {
        DB::table('tbl_transaksipm')
            ->where('id_transaksipm', $id_transaksipm)
            ->update($data);
    }

    public function DeleteData($id_transaksipm)
    {
        DB::table('tbl_transaksipm')
            ->where('id_transaksipm', $id_transaksipm)
            ->delete();
    }
}
