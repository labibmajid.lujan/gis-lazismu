<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JenisDModel extends Model
{
    protected $table = "tbl_jenisdonatur";

    public function AllData()
    {
        return DB::table('tbl_jenisdonatur')
            ->get();
    }

    //menangkap data dari fungsi insert
    public function InsertData($data)
    {
        DB::table('tbl_jenisdonatur')
            ->insert($data);
    }

    //ambil data untuk diedit
    public function DetailData($id_jenisdonatur)
    {
        return DB::table('tbl_jenisdonatur')
            ->where('id_jenisdonatur', $id_jenisdonatur)
            ->first(); //berdasarkan idjenisdonatur
    }

    public function UpdateData($id_jenisdonatur, $data)
    {
        DB::table('tbl_jenisdonatur')
            ->where('id_jenisdonatur', $id_jenisdonatur)
            ->update($data);
    }

    public function DeleteData($id_jenisdonatur)
    {
        DB::table('tbl_jenisdonatur')
            ->where('id_jenisdonatur', $id_jenisdonatur)
            ->delete();
    }

    public function jenis_transaksi()
    {
        return $this->hasMany(TransaksiModel::class, 'id_jenisdonatur', 'id_jenisdonatur');
    }
}
