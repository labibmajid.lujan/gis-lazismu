<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DonaturModel extends Model
{

    protected $table = 'tbl_pemetaand';

    public function data_pemetaan()
    {
        return $this->belongsTo(DonaturModel::class, 'id_dd', 'id_dd');
    }

    public function AllData()
    {
        return DB::table('tbl_pemetaand')
            //fungsi join
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
            ->get();
    }

    public function InserData($data)
    {
        DB::table('tbl_pemetaand')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id_donatur)
    {
        return DB::table('tbl_pemetaand')
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
            ->where('id_donatur', $id_donatur)
            ->first(); //berdasarkan id_donatur
    }

    public function UpdateData($id_donatur, $data)
    {
        DB::table('tbl_pemetaand')
            ->where('id_donatur', $id_donatur)
            ->update($data);
    }

    public function DeleteData($id_donatur)
    {
        DB::table('tbl_pemetaand')
            ->where('id_donatur', $id_donatur)
            ->delete();
    }
}
