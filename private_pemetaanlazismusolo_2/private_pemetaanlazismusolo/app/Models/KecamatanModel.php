<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class KecamatanModel extends Model
{
    public function AllData()
    {
        return DB::table('tbl_kecamatan')
            ->get();
    }

    public function InserData($data)
    {
        DB::table('tbl_kecamatan')
            ->insert($data);
    }

    //ambil data untuk diedit
    public function DetailData($id_kecamatan)
    {
        return DB::table('tbl_kecamatan')
            ->where('id_kecamatan', $id_kecamatan)
            ->first(); //berdasarkan idkecamatan
    }

    public function UpdateData($id_kecamatan, $data)
    {
        DB::table('tbl_kecamatan')
            ->where('id_kecamatan', $id_kecamatan)
            ->update($data);
    }


    public function DeleteData($id_kecamatan)
    {
        DB::table('tbl_kecamatan')
            ->where('id_kecamatan', $id_kecamatan)
            ->delete();
    }
    // use HasFactory;
    // protected $table = 'kecamatan';
    // protected $guarded = [];
    // // protected $guarded = [];
}
