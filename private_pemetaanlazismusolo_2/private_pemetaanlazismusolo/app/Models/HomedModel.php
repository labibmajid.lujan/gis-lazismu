<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HomedModel extends Model
{
    public function DataKecamatan()
    {
        return DB::table('tbl_kecamatan')
            ->get();
    }

    //data donatur untuk ditampilakan didashboard
    public function AllDataDonatur()
    {
        return DB::table('tbl_pemetaand')
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
            ->get();
    }

    //memanggil agar data kecamatan pada map bisa tampil perkecamatan
    public function DetailKecamatan($id_kecamatan)
    {
        return DB::table('tbl_kecamatan')
            ->where('id_kecamatan', $id_kecamatan)->first();
    }

    public function DataJenis()
    {
        return DB::table('tbl_jenisdonatur')
            ->get();
    }

    public function DataTransaksi()
    {
        return DB::table('tbl_transaksi')
            ->get();
    }

    public function DataDonatur($id_kecamatan)
    {
        return DB::table('tbl_pemetaand')
            //fungsi join
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
            ->where('tbl_pemetaand.id_kecamatan', $id_kecamatan) //fungsi tabel menampilkan data donatur berdasarkan id_kecamatan 
            ->get();
    }

    public function SemuaDataDonatur() //memanggil agar data donatur bisa tampil menggunakan marker pada map
    {
        return DB::table('tbl_pemetaand')
            //fungsi join
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
            ->get();
    }

    public function DataDonaturJenis($id_jenisdonatur)
    {
        return DB::table('tbl_pemetaand')
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
            ->where('tbl_pemetaand.id_jenisdonatur', $id_jenisdonatur)
            ->get();
    }

    public function DetailJenis($id_jenisdonatur)
    {
        return DB::table('tbl_jenisdonatur')
            ->where('id_jenisdonatur', $id_jenisdonatur)->first();
    }

    // public function DetailDonatur($id_donatur)
    // {
    //     $data_donatur = DDModel::where('id_dd', $id_donatur)->first();
    //     return $data_donatur;
    // dd($data_donatur);
    // return DB::table('tbl_datadonatur')
    //     ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_pemetaand.id_dd')
    //     ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_pemetaand.id_jenisdonatur')
    //     ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaand.id_kecamatan')
    //     ->where('id_donatur', $id_donatur)->first();
    // }

    public function DataDonaturTransaksi($id_dd)
    {
        return DB::table('tbl_transaksi')
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_transaksi.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_transaksi.id_jenisdonatur')
            ->where('tbl_transaksi.id_dd', $id_dd)
            ->get();
    }
}
