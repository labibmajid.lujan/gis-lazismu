<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DDModel extends Model
{
    // protected $table = 'tbl_datadonatur';

    // public function data_pemetaan()
    // {
    //     return $this->hasMany(DonaturModel::class, 'id_dd', 'id_dd');
    // }

    public function AllData()
    {
        return DB::table('tbl_datadonatur')
            //fungsi join
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_datadonatur.id_kecamatan')
            ->get();
    }

    public function InsertData($data)
    {
        DB::table('tbl_datadonatur')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id_dd)
    {
        return DB::table('tbl_datadonatur')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_datadonatur.id_kecamatan')
            ->where('id_dd', $id_dd)
            ->first(); //berdasarkan id_dd
    }

    public function UpdateData($id_dd, $data)
    {
        DB::table('tbl_datadonatur')
            ->where('id_dd', $id_dd)
            ->update($data);
    }

    public function DeleteData($id_dd)
    {
        DB::table('tbl_datadonatur')
            ->where('id_dd', $id_dd)
            ->delete();
    }
}
