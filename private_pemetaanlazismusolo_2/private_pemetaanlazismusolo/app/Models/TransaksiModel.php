<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TransaksiModel extends Model
{
    protected $table = "tbl_transaksi";

    public function AllData()
    {
        return DB::table('tbl_transaksi')
            //fungsi join
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_transaksi.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_transaksi.id_jenisdonatur')
            ->get();
    }

    public function InsertData($data)
    {
        DB::table('tbl_transaksi')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id_transaksi)
    {
        return DB::table('tbl_transaksi')
            ->join('tbl_datadonatur', 'tbl_datadonatur.id_dd', '=', 'tbl_transaksi.id_dd')
            ->join('tbl_jenisdonatur', 'tbl_jenisdonatur.id_jenisdonatur', '=', 'tbl_transaksi.id_jenisdonatur')
            ->where('id_transaksi', $id_transaksi)
            ->first(); //berdasarkan id_donatur
    }

    public function UpdateData($id_transaksi, $data)
    {
        DB::table('tbl_transaksi')
            ->where('id_transaksi', $id_transaksi)
            ->update($data);
    }

    public function DeleteData($id_transaksi)
    {
        DB::table('tbl_transaksi')
            ->where('id_transaksi', $id_transaksi)
            ->delete();
    }

    public function jenis_transaksi()
    {
        return $this->belongsTo(JenisDModel::class, 'id_jenisdonatur', 'id_jenisdonatur');
    }
}
