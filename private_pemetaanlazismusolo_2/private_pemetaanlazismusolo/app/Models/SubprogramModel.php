<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SubprogramModel extends Model
{
    protected $table = "tbl_subprogram";
    public function AllData()
    {
        return DB::table('tbl_subprogram')
            //fungsi join
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_subprogram.id_program')
            ->get();
    }

    public function InsertData($data)
    {
        DB::table('tbl_subprogram')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id_subprogram)
    {
        return DB::table('tbl_subprogram')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_subprogram.id_program')
            ->where('id_subprogram', $id_subprogram)
            ->first(); //berdasarkan id_subprogram
    }

    public function UpdateData($id_subprogram, $data)
    {
        DB::table('tbl_subprogram')
            ->where('id_subprogram', $id_subprogram)
            ->update($data);
    }

    public function DeleteData($id_subprogram)
    {
        DB::table('tbl_subprogram')
            ->where('id_subprogram', $id_subprogram)
            ->delete();
    }
}
