<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PMModel extends Model
{
    public function AllData()
    {
        return DB::table('tbl_pemetaanpm')
            //fungsi join
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_pemetaanpm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_pemetaanpm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_pemetaanpm.id_subprogram')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaanpm.id_kecamatan')
            ->get();
    }

    public function InsertData($data)
    {
        DB::table('tbl_pemetaanpm')
            ->insert($data);
    }

    public function DetailData($id_pm)
    {
        return DB::table('tbl_pemetaanpm')
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_pemetaanpm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_pemetaanpm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_pemetaanpm.id_subprogram')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaanpm.id_kecamatan')
            ->where('id_pm', $id_pm)
            ->first(); //berdasarkan id_pm
    }

    public function UpdateData($id_pm, $data)
    {
        DB::table('tbl_pemetaanpm')
            ->where('id_pm', $id_pm)
            ->update($data);
    }

    public function DeleteData($id_pm)
    {
        DB::table('tbl_pemetaanpm')
            ->where('id_pm', $id_pm)
            ->delete();
    }
}
