<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DPMModel extends Model
{
    public function AllData()
    {
        return DB::table('tbl_datapm')
            //fungsi join
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_datapm.id_kecamatan')
            ->get();
    }

    public function InsertData($data)
    {
        DB::table('tbl_datapm')
            ->insert($data);
    }

    //ambil data untuk diedit kemudian dipassing ke form edit
    public function DetailData($id_dpm)
    {
        return DB::table('tbl_datapm')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_datapm.id_kecamatan')
            ->where('id_dpm', $id_dpm)
            ->first(); //berdasarkan id_dd
    }

    public function UpdateData($id_dpm, $data)
    {
        DB::table('tbl_datapm')
            ->where('id_dpm', $id_dpm)
            ->update($data);
    }

    public function DeleteData($id_dpm)
    {
        DB::table('tbl_datapm')
            ->where('id_dpm', $id_dpm)
            ->delete();
    }
}
