<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class HomePMModel extends Model
{
    public function DataKecamatan()
    {
        return DB::table('tbl_kecamatan')
            ->get();
    }

    public function DataSubprogram()
    {
        return DB::table('tbl_subprogram')
            ->get();
    }

    public function Dataprogram()
    {
        return DB::table('tbl_program')
            ->get();
    }

    public function DataTransaksiPM()
    {
        return DB::table('tbl_transaksipm')
            ->get();
    }

    public function DataDPM()
    {
        return DB::table('tbl_datapm')
            ->get();
    }

    //memanggil agar data kecamatan pada map bisa tampil perkecamatan
    public function DetailKecamatan($id_kecamatan)
    {
        return DB::table('tbl_kecamatan')
            ->where('id_kecamatan', $id_kecamatan)->first();
    }

    public function DetailProgram($id_program)
    {
        return DB::table('tbl_program')
            ->where('id_program', $id_program)->first();
    }

    public function DataPM($id_kecamatan)
    {
        return DB::table('tbl_pemetaanpm')
            //fungsi join
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_pemetaanpm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_pemetaanpm.id_program')
            //->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_pemetaanpm.id_subprogram')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaanpm.id_kecamatan')
            ->where('tbl_pemetaanpm.id_kecamatan', $id_kecamatan) //fungsi tabel menampilkan data pm berdasarkan id_kecamatan 
            ->get();
    }

    //memanggil data PM per Program
    public function DataPMProgram($id_program)
    {
        return DB::table('tbl_pemetaanpm')
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_pemetaanpm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_pemetaanpm.id_program')
            //->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_pemetaanpm.id_subprogram')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaanpm.id_kecamatan')
            ->where('tbl_pemetaanpm.id_program', $id_program)
            ->get();
    }

    public function SemuaDataPM() //memanggil agar data pm bisa tampil menggunakan marker pada map
    {
        return DB::table('tbl_pemetaanpm')
            //fungsi join
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_pemetaanpm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_pemetaanpm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_pemetaanpm.id_subprogram')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaanpm.id_kecamatan')
            ->get();
    }

    public function DetailPM($id_pm)
    {
        return DB::table('tbl_pemetaanpm')
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_pemetaanpm.id_dpm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_pemetaanpm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_pemetaanpm.id_subprogram')
            ->join('tbl_kecamatan', 'tbl_kecamatan.id_kecamatan', '=', 'tbl_pemetaanpm.id_kecamatan')
            ->where('id_pm', $id_pm)->first();
    }

    public function DataDPMTransaksi($id_dpm)
    {
        return DB::table('tbl_transaksipm')
            ->join('tbl_datapm', 'tbl_datapm.id_dpm', '=', 'tbl_transaksipm.id_dpm')
            // ->join('tbl_pemetaanpm', 'tbl_pemetaanpm.id_pm', '=', 'tbl_transaksipm.id_pm')
            ->join('tbl_program', 'tbl_program.id_program', '=', 'tbl_transaksipm.id_program')
            ->join('tbl_subprogram', 'tbl_subprogram.id_subprogram', '=', 'tbl_transaksipm.id_subprogram')
            ->where('tbl_transaksipm.id_dpm', $id_dpm)
            ->get();
    }
}
