<?php

namespace App\Http\Controllers;

use App\Models\JenisDModel;
use Illuminate\Http\Request;

class JenisDController extends Controller
{
    private $JenisDModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->JenisDModel = new JenisDModel();
    }

    public function index()
    {
        $data = [
            'jenis' => $this->JenisDModel->AllData(),
        ];
        return view('jenisdonatur.jenisd', $data);
    }

    public function add()
    {
        $data = [];
        return view('jenisdonatur.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'jenis_donatur' => 'required',
                'icon' => 'required',
            ],
            [
                'jenis_donatur.required' => 'wajib diisi !!!',
                'icon.required' => 'wajib diisi !!!',
            ]
        );

        //
        $file = request()->icon; //merequest form dengan nama icon
        $filename = $file->getClientOriginalName(); //dengan nama file asli ketika masuk folder
        // $file->move(public_path('icon'), $filename); //kemudian di move dalam folder icon
        $file->move(base_path() . '/../public_html/upload/icon', $filename); //kemudian di move dalam folder icon
        
        //mengirim variabel data ke JenisModel
        $data = [
            'jenis_donatur' => request()->jenis_donatur,
            'icon' => $filename,
        ];
        $this->JenisDModel->InsertData($data);
        return redirect()->route('jenis-donatur')->with('pesan', 'Data berhasil di simpan!!!');
    }

    public function edit($id_jenisdonatur)
    {
        $data = [
            'jenis_donatur' => $this->JenisDModel->DetailData($id_jenisdonatur),
        ]; //mempassing data ke view edit
        return view('jenisdonatur.edit', $data);
    }

    public function update($id_jenisdonatur)
    {
        request()->validate(
            [
                'jenis_donatur' => 'required',
            ],
            [
                'jenis_donatur.required' => 'wajib diisi !!!',
            ]
        );

        if (request()->icon <> "") {
            //menghapus icon lama di dalam folder setelah diupdate
            $jenis_donatur = $this->JenisDModel->DetailData($id_jenisdonatur);
            if ($jenis_donatur->icon <> "") {
                // unlink(public_path('icon') . '/' . $jenis_donatur->icon);
                unlink(base_path() . '/../public_html/upload/icon' . '/' . $jenis_donatur->icon);
            }

            //jika ingin ganti icon maka 
            $file = request()->icon; //merequest form dengan nama icon
            $filename = $file->getClientOriginalName(); //dengan nama file asli ketika masuk folder
            // $file->move(public_path('icon'), $filename); //kemudian di move dalam folder icon
            $file->move(base_path() . '/../public_html/upload/icon', $filename); //kemudian di move dalam folder icon

            //mengirim variabel data ke JenisModel
            $data = [
                'jenis_donatur' => request()->jenis_donatur,
                'icon' => $filename,
            ];
            $this->JenisDModel->UpdateData($id_jenisdonatur, $data);
        } else {
            //jika tidak ganti icon maka
            $data = [
                'jenis_donatur' => request()->jenis_donatur,
            ];
            $this->JenisDModel->UpdateData($id_jenisdonatur, $data); //ambil data dari fungsi update
        }
        return redirect()->route('jenis-donatur')->with('pesan', 'Data berhasil di simpan!!!');
    }

    public function delete($id_jenisdonatur)
    {
        //menghapus icon lama
        $jenis_donatur = $this->JenisDModel->DetailData($id_jenisdonatur);
        if ($jenis_donatur->icon <> "") {
            // unlink(public_path('icon') . '/' . $jenis_donatur->icon);
            unlink(base_path() . '/../public_html/upload/icon' . '/' . $jenis_donatur->icon);
        }

        $this->JenisDModel->DeleteData($id_jenisdonatur);
        return redirect()->route('jenis-donatur')->with('pesan', 'Data Berhasil ter-delete');
    }
}
