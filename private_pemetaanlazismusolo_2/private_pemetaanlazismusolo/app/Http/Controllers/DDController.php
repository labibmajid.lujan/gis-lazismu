<?php

namespace App\Http\Controllers;

use App\Models\DDModel;
use App\Models\KecamatanModel;
use Illuminate\Http\Request;

class DDController extends Controller
{
    private $DDModel;
    private $KecamatanModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->DDModel = new DDModel();
        $this->KecamatanModel = new KecamatanModel();
    }

    public function index()
    {
        $data = [
            'dd' => $this->DDModel->AllData(),
        ];
        return view('datadonatur.index', $data);
    }

    public function add()
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData(),    //use data kecamatan
        ];
        return view('datadonatur.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'nama_dd' => 'required',
                'id_kecamatan' => 'required',
                'alamat_dd' => 'required',
                'notelp_dd' => 'required',
            ],
            [
                'nama_dd.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'alamat_dd.required' => 'wajib diisi !!!',
                'notelp_dd.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_dd' => Request()->nama_dd,
            'id_kecamatan' => Request()->id_kecamatan,
            'alamat_dd' => Request()->alamat_dd,
            'notelp_dd' => Request()->notelp_dd,
        ];

        //menyimpan ke modelkecamatan
        $this->DDModel->InsertData($data);
        return redirect()->route('dd')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_dd)
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData(),
            'dd' => $this->DDModel->DetailData($id_dd),
        ];
        return view('datadonatur.edit', $data);
    }

    public function update($id_dd)
    {
        request()->validate(
            [
                'nama_dd' => 'required',
                'id_kecamatan' => 'required',
                'alamat_dd' => 'required',
                'notelp_dd' => 'required',
            ],
            [
                'nama_dd.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'alamat_dd.required' => 'wajib diisi !!!',
                'notelp_dd.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_dd' => Request()->nama_dd,
            'id_kecamatan' => Request()->id_kecamatan,
            'alamat_dd' => Request()->alamat_dd,
            'notelp_dd' => Request()->notelp_dd,
        ];
        $this->DDModel->UpdateData($id_dd, $data);
        return redirect()->route('dd')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id_dd)
    {
        $this->DDModel->DeleteData($id_dd);
        return redirect()->route('dd')->with('pesan', 'Data Berhasil ter-delete');
    }
}
