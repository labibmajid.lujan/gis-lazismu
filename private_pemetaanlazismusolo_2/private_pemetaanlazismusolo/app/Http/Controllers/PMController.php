<?php

namespace App\Http\Controllers;

use App\Models\DPMModel;
use App\Models\KecamatanModel;
use App\Models\PMModel;
use App\Models\ProgramPMModel;
use App\Models\SubprogramModel;
use Illuminate\Http\Request;

class PMController extends Controller
{
    private $PMModel;
    private $ProgramPMModel;
    private $KecamatanModel;
    private $SubprogramModel;
    private $DPMModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->PMModel = new PMModel();
        $this->ProgramPMModel = new ProgramPMModel();
        $this->KecamatanModel = new KecamatanModel();
        $this->SubprogramModel = new SubprogramModel();
        $this->DPMModel = new DPMModel();
    }

    public function index()
    {
        $data = [
            'pm' => $this->PMModel->AllData(),
        ];
        return view('pm.indexpm', $data);
    }

    public function add()
    {
        $data = [
            'program' => $this->ProgramPMModel->AllData(),
            'kecamatan' => $this->KecamatanModel->AllData(),    //use data kecamatan
            'subprogram' => $this->SubprogramModel->AllData(),           //use data subprogram
            'dpm' => $this->DPMModel->AllData(),
        ];
        return view('pm.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'id_dpm' => 'required',
                'id_program' => 'required',
                'id_subprogram' => 'required',
                'id_kecamatan' => 'required',
                'posisi_pm' => 'required',
                'deskripsi_pm' => 'required',
                'foto_pm' => 'required',
            ],
            [
                'id_dpm.required' => 'wajib diisi !!!',
                'id_program.required' => 'wajib diisi !!!',
                'id_subprogram.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'posisi_pm.required' => 'wajib diisi !!!',
                'deskripsi_pm.required' => 'wajib diisi !!!',
                'foto_pm.required' => 'wajib diisi !!!',
            ]
        );

        $file = request()->foto_pm; //merequest form dengan nama icon
        $filename = $file->getClientOriginalName(); //dengan nama file asli ketika masuk folder
        // $file->move(public_path('fotopm'), $filename); //kemudian di move dalam folder icon
        $file->move(base_path() . '/../public_html/upload/fotopm', $filename); //kemudian di move dalam folder icon

        $data = [
            'id_dpm' => Request()->id_dpm,
            'id_program' => Request()->id_program,
            'id_subprogram' => Request()->id_subprogram,
            'id_kecamatan' => Request()->id_kecamatan,
            'posisi_pm' => Request()->posisi_pm,
            'deskripsi_pm' => Request()->deskripsi_pm,
            'foto_pm' => $filename,
        ];

        //menyimpan ke modelkecamatan
        $this->PMModel->InsertData($data);
        return redirect()->route('penerima-manfaat')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_pm)
    {
        $data = [
            'program' => $this->ProgramPMModel->AllData(),
            'kecamatan' => $this->KecamatanModel->AllData(),    //use data kecamatan
            'subprogram' => $this->SubprogramModel->AllData(),           //use data subprogram
            'dpm' => $this->DPMModel->AllData(),
            'pm' => $this->PMModel->DetailData($id_pm),
        ];
        
        return view('pm.edit', $data);
    }

    public function update($id_pm)
    {
        request()->validate(
            [
                'id_dpm' => 'required',
                'id_program' => 'required',
                'id_subprogram' => 'required',
                'id_kecamatan' => 'required',
                'posisi_pm' => 'required',
                'deskripsi_pm' => 'required',
            ],
            [
                'id_dpm.required' => 'wajib diisi !!!',
                'id_program.required' => 'wajib diisi !!!',
                'id_subprogram.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'posisi_pm.required' => 'wajib diisi !!!',
                'deskripsi_pm.required' => 'wajib diisi !!!',
            ]
        );

        if (request()->foto_pm <> "") {
            //menghapus foto lama di dalam folder setelah diupdate
            $penerima_manfaat = $this->PMModel->DetailData($id_pm);
            if ($penerima_manfaat->foto_pm <> "") {
                unlink(base_path() . '/../public_html/upload/fotopm' . '/' . $penerima_manfaat->foto_pm);
                // $file->move(base_path() . '/../public_html/upload/fotopm', $filename);
            }

            //jika ingin ganti icon maka 
            $file = request()->foto_pm; //merequest form dengan nama foto_pm
            $filename = $file->getClientOriginalName(); //dengan nama file asli ketika masuk folder
            $file->move(base_path() . '/../public_html/upload/fotopm', $filename); //kemudian di move dalam folder fotopm
            // $file->move(base_path() . '/../public_html/upload/fotopm', $filename);
            
            //mengirim variabel data ke JenisModel
            $data = [
                'id_dpm' => Request()->id_dpm,
                'id_program' => Request()->id_program,
                'id_subprogram' => Request()->id_subprogram,
                'id_kecamatan' => Request()->id_kecamatan,
                'posisi_pm' => Request()->posisi_pm,
                'deskripsi_pm' => Request()->deskripsi_pm,
                'foto_pm' => $filename,
            ];
            $this->PMModel->UpdateData($id_pm, $data);
        } else {
            //jika tidak ganti icon maka
            $data = [
                'pm' => request()->penerima_manfaat,
            ];
            $this->PMModel->UpdateData($id_pm, $data); //ambil data dari fungsi update
        }
        return redirect()->route('penerima-manfaat')->with('pesan', 'Data berhasil di simpan!!!');
    }

    public function delete($id_pm)
    {
        //menghapus icon lama
        $penerima_manfaat = $this->PMModel->DetailData($id_pm);
        if ($penerima_manfaat->foto_pm <> "") {
            unlink(base_path() . '/../public_html/upload/fotopm' . '/' . $penerima_manfaat->foto_pm);
            
        }

        $this->PMModel->DeleteData($id_pm);
        return redirect()->route('penerima-manfaat')->with('pesan', 'Data Berhasil ter-delete');
    }

    public function programSearch(Request $request)
    {
        $sub_program = SubprogramModel::where('id_program', $request->id_program)->pluck('nama_subprogram', 'id_subprogram');
        return response()->json([
            'sub_program' => $sub_program
        ]);
    }
}
