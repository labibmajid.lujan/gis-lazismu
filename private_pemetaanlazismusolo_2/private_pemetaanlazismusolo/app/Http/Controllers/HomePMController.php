<?php

namespace App\Http\Controllers;

use App\Models\HomePMModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomePMController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $HomePMModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->HomePMModel = new HomePMModel();
    }

    public function pm_map()
    {

        $data = [
            'dpm' => $this->HomePMModel->DataDPM(),
            'kecamatan' => $this->HomePMModel->DataKecamatan(),   //menampilkan batas kecamatan pada map pm
            'pm' => $this->HomePMModel->SemuaDataPM(), //memanggil agar data donatur bisa tampil menggunakan marker pada map
            'program' => $this->HomePMModel->Dataprogram(),
            'subprogram' => $this->HomePMModel->DataSubprogram(),
        ];
        return view('mappm.mappm', $data);
    }

    //menampilkan map perkecamatan PM
    public function mapkecamatanpm($id_kecamatan)
    {
        $kec = $this->HomePMModel->DetailKecamatan($id_kecamatan); //variabel 'kec' agar dapat dipanggil di mapkecamatan 
        $data = [
            'kecamatan' => $this->HomePMModel->DataKecamatan(),
            'pm' => $this->HomePMModel->DataPM($id_kecamatan),
            'program' => $this->HomePMModel->Dataprogram(),
            'kec' => $kec,
        ];
        return view('mappm.mapkecpm', $data);
    }

    //menampilkan map berdasarkan program
    public function mapprogrampm($id_program)
    {
        $prog = $this->HomePMModel->DetailProgram($id_program);
        $data = [
            'kecamatan' => $this->HomePMModel->DataKecamatan(),
            'pm' => $this->HomePMModel->DataPMProgram($id_program), //'DataPMProgram' dibuat fungsi pada model
            'program' => $this->HomePMModel->Dataprogram(),
            'prog' => $prog,
        ];
        return view('mappm.mapprogpm', $data);
    }

    public function detaildatapm($id_pm)
    {
        $pm = $this->HomePMModel->DetailPM($id_pm); //mengatur fungsi DetailPM pada homePMmodel
        $data = [
            'kecamatan' => $this->HomePMModel->DataKecamatan(),   //menampilkan batas kecamatan pada map pm
            'pm' => $this->HomePMModel->SemuaDataPM(), //memanggil agar data donatur bisa tampil menggunakan marker pada map
            'program' => $this->HomePMModel->Dataprogram(),
            'subprogram' => $this->HomePMModel->DataSubprogram(),
            // 'jmltransaksipm' => DB::table('tbl_transaksidpm')
            //     ->where('id_dpm', $id_dpm)
            //     ->count(), //menghitung data pm berdasarkan id
            'pm' => $pm,
        ];
        return view('mappm.detailpm', $data);
    }

    public function rekaptransaksipm($id_dpm)
    {
        $data = [
            'transaksipm' => $this->HomePMModel->DataTransaksiPM(),
            // 'kecamatan' => $this->HomedModel->DataKecamatan(),
            'dpm' => $this->HomePMModel->DataDPMTransaksi($id_dpm),
            'program' => $this->HomePMModel->Dataprogram(),
            'subprogram' => $this->HomePMModel->DataSubprogram(),
        ];
        return view('datapm.rekaptransaksi', $data);
    }
}
