<?php

namespace App\Http\Controllers;

use App\Models\DonaturModel;
use Illuminate\Http\Request;
use App\Models\HomedModel;
use App\Models\JenisDModel;
use App\Models\TransaksiModel;
use App\Models\TransaksipmModel;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $HomedModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->HomedModel = new HomedModel();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data = [
            'datadonatur' => DB::table('tbl_datadonatur')->count(), //menghitung data donatur
            'transaksi' => DB::table('tbl_transaksi')->count(), //menghitung data transaksi
            'transaksipm' => DB::table('tbl_transaksipm')->count(), //menghitung data transaksi
            'datapm' => DB::table('tbl_datapm')->count(), //menghitung data pm
            'jumlah_shadaqah' => TransaksiModel::where('id_jenisdonatur', 11)->get()->count(),
            'jumlah_infaq' => TransaksiModel::where('id_jenisdonatur', 10)->get()->count(),
            'jumlah_zakat' => TransaksiModel::where('id_jenisdonatur', 9)->get()->count(),
            'jumlah_pend' => TransaksipmModel::where('id_program', 7)->get()->count(),
            'jumlah_kes' => TransaksipmModel::where('id_program', 8)->get()->count(),
            'jumlah_eko' => TransaksipmModel::where('id_program', 9)->get()->count(),
            'jumlah_sosdak' => TransaksipmModel::where('id_program', 10)->get()->count(),
            'jumlah_kem' => TransaksipmModel::where('id_program', 11)->get()->count(),
            'jumlah_ling' => TransaksipmModel::where('id_program', 12)->get()->count(),
        ];
        return view('home', $data);
    }

    public function donatur_map()
    {

        $data = [
            'kecamatan' => $this->HomedModel->DataKecamatan(),   //menampilkan batas kecamatan pada map donatur
            'donatur' => $this->HomedModel->SemuaDataDonatur(), //memanggil agar data donatur bisa tampil menggunakan marker pada map
            'jenis' => $this->HomedModel->DataJenis(),
        ];
        return view('jenisdonatur.donatur-map', $data);
    }

    //menampilkan map perkecamatan
    public function mapkecamatan($id_kecamatan)
    {
        $kec = $this->HomedModel->DetailKecamatan($id_kecamatan); //variabel 'kec' agar dapat dipanggil di mapkecamatan 
        $data = [
            'kecamatan' => $this->HomedModel->DataKecamatan(),
            'donatur' => $this->HomedModel->DataDonatur($id_kecamatan),
            'jenis' => $this->HomedModel->DataJenis(),
            'kec' => $kec,
        ];
        return view('donatur.mapkecamatan', $data);
    }

    public function mapjenis($id_jenisdonatur)
    {
        $jen = $this->HomedModel->DetailJenis($id_jenisdonatur);
        $data = [
            'kecamatan' => $this->HomedModel->DataKecamatan(),
            'donatur' => $this->HomedModel->DataDonaturJenis($id_jenisdonatur), //'DataDonaturJenis' dibuat fungsi pada model
            'jenis' => $this->HomedModel->DataJenis(),
            'jen' => $jen,
        ];
        return view('donatur.mapjenis', $data);
    }

    // public function detaildatadonatur($id_dd)
    // {
    //     $data = $this->HomedModel->DetailDonatur($id_dd); //mengatur fungsi DetailDonatur pada homedmodel
    // dd($data->data_pemetaan);
    // $data = [
    //     'kecamatan' => $this->HomedModel->DataKecamatan(),
    //     'jenis' => $this->HomedModel->DataJenis(),
    //     'dd' => $dd,
    //     'jmltransaksi' => DB::table('tbl_transaksi')
    //         ->where('id_dd', $id_dd)
    //         ->count(), //menghitung data transaksi
    // ];
    //     return view('donatur.detaildonatur', compact('data'));
    // }

    public function rekaptransaksi($id_dd)
    {
        $data = [
            'transaksi' => $this->HomedModel->DataTransaksi(),
            // 'kecamatan' => $this->HomedModel->DataKecamatan(),
            'dd' => $this->HomedModel->DataDonaturTransaksi($id_dd),
            'jenis' => $this->HomedModel->DataJenis(),
        ];
        return view('datadonatur.rekaptransaksi', $data);
    }
}
