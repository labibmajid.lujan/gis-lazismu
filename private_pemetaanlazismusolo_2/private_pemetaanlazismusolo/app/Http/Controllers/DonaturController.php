<?php

namespace App\Http\Controllers;

use App\Models\DDModel;
use App\Models\DonaturModel;
use App\Models\JenisDModel;
use App\Models\KecamatanModel;
use Illuminate\Http\Request;

class DonaturController extends Controller
{
    private $DonaturModel;
    private $DDModel;
    private $KecamatanModel;
    private $JenisDModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->DonaturModel = new DonaturModel();
        $this->KecamatanModel = new KecamatanModel();
        $this->JenisDModel = new JenisDModel();
        $this->DDModel = new DDModel();
    }

    public function index()
    {
        $data = [
            'donatur' => $this->DonaturModel->AllData(),
        ];
        return view('donatur.indexd', $data);
    }

    public function add()
    {
        $data = [
            'dd' => $this->DDModel->AllData(),
            'kecamatan' => $this->KecamatanModel->AllData(),    //use data kecamatan
            'jenis' => $this->JenisDModel->AllData(),           //use data jenis donatur
        ];
        return view('donatur.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'id_dd' => 'required',
                'id_jenisdonatur' => 'required',
                'id_kecamatan' => 'required',
                'posisi' => 'required',
            ],
            [
                'id_dd.required' => 'wajib diisi !!!',
                'id_jenisdonatur.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'posisi.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_dd' => Request()->id_dd,
            'id_jenisdonatur' => Request()->id_jenisdonatur,
            'id_kecamatan' => Request()->id_kecamatan,
            'posisi' => Request()->posisi,
        ];

        //menyimpan ke modelkecamatan
        $this->DonaturModel->InserData($data);
        return redirect()->route('donatur')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_donatur)
    {
        $data = [
            'dd' => $this->DDModel->AllData(),
            'kecamatan' => $this->KecamatanModel->AllData(),
            'jenis' => $this->JenisDModel->AllData(),
            'donatur' => $this->DonaturModel->DetailData($id_donatur),
        ];
        return view('donatur.edit', $data);
    }

    public function update($id_donatur)
    {
        request()->validate(
            [
                'id_dd' => 'required',
                'id_jenisdonatur' => 'required',
                'id_kecamatan' => 'required',
                'posisi' => 'required',
            ],
            [
                'id_dd.required' => 'wajib diisi !!!',
                'id_jenisdonatur.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'posisi.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_dd' => Request()->id_dd,
            'id_jenisdonatur' => Request()->id_jenisdonatur,
            'id_kecamatan' => Request()->id_kecamatan,
            'posisi' => Request()->posisi,
        ];
        $this->DonaturModel->UpdateData($id_donatur, $data);
        return redirect()->route('donatur')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id_donatur)
    {
        $this->DonaturModel->DeleteData($id_donatur);
        return redirect()->route('donatur')->with('pesan', 'Data Berhasil ter-delete');
    }
}
