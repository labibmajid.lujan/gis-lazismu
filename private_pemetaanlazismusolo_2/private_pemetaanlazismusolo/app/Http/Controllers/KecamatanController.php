<?php

namespace App\Http\Controllers;

use App\Models\KecamatanModel;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    private $KecamatanModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->KecamatanModel = new KecamatanModel();
    }

    public function index()
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData()
        ];
        return view('kecamatan.indexkec', $data);
    }

    public function add()
    {
        $data = [];
        return view('kecamatan.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'kecamatan' => 'required',
                'warna' => 'required',
                'geojson' => 'required',
            ],
            [
                'kecamatan.required' => 'wajib diisi !!!',
                'warna.required' => 'wajib diisi !!!',
                'geojson.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'kecamatan' => Request()->kecamatan,
            'warna' => Request()->warna,
            'geojson' => Request()->geojson,
        ];

        //menyimpan ke modelkecamatan
        $this->KecamatanModel->InserData($data);
        return redirect()->route('kecamatan')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_kecamatan)
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->DetailData($id_kecamatan),
        ];
        return view('kecamatan.edit', $data);
    }

    public function update($id_kecamatan)
    {
        request()->validate(
            [
                'kecamatan' => 'required',
                'warna' => 'required',
                'geojson' => 'required',
            ],
            [
                'kecamatan.required' => 'wajib diisi !!!',
                'warna.required' => 'wajib diisi !!!',
                'geojson.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'kecamatan' => Request()->kecamatan,
            'warna' => Request()->warna,
            'geojson' => Request()->geojson,
        ];

        $this->KecamatanModel->UpdateData($id_kecamatan, $data);
        return redirect()->route('kecamatan')->with('pesan', 'Data Berhasil ter-update');
    }

    public function delete($id_kecamatan)
    {
        $this->KecamatanModel->DeleteData($id_kecamatan);
        return redirect()->route('kecamatan')->with('pesan', 'Data Berhasil ter-delete');
    }
}
