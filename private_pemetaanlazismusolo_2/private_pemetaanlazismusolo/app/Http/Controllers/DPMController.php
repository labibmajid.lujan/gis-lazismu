<?php

namespace App\Http\Controllers;

use App\Models\DPMModel;
use App\Models\KecamatanModel;
use App\Models\SubprogramModel;
use Illuminate\Http\Request;

class DPMController extends Controller
{
    private $DPMModel;
    private $KecamatanModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->DPMModel = new DPMModel();
        $this->KecamatanModel = new KecamatanModel();
    }

    public function index()
    {
        $data = [
            'dpm' => $this->DPMModel->AllData(),
        ];
        return view('datapm.index', $data);
    }

    public function add()
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData(),    //use data kecamatan
        ];
        return view('datapm.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'nama_dpm' => 'required',
                'id_kecamatan' => 'required',
                'alamat_dpm' => 'required',
                'notelp_dpm' => 'required',
            ],
            [
                'nama_dpm.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'alamat_dpm.required' => 'wajib diisi !!!',
                'notelp_dpm.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_dpm' => Request()->nama_dpm,
            'id_kecamatan' => Request()->id_kecamatan,
            'alamat_dpm' => Request()->alamat_dpm,
            'notelp_dpm' => Request()->notelp_dpm,
        ];

        //menyimpan ke modelkecamatan
        $this->DPMModel->InsertData($data);
        return redirect()->route('dpm')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_dpm)
    {
        $data = [
            'kecamatan' => $this->KecamatanModel->AllData(),
            'dpm' => $this->DPMModel->DetailData($id_dpm),
        ];
        return view('datapm.edit', $data);
    }

    public function update($id_dpm)
    {
        request()->validate(
            [
                'nama_dpm' => 'required',
                'id_kecamatan' => 'required',
                'alamat_dpm' => 'required',
                'notelp_dpm' => 'required',
            ],
            [
                'nama_dpm.required' => 'wajib diisi !!!',
                'id_kecamatan.required' => 'wajib diisi !!!',
                'alamat_dpm.required' => 'wajib diisi !!!',
                'notelp_dpm.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'nama_dpm' => Request()->nama_dpm,
            'id_kecamatan' => Request()->id_kecamatan,
            'alamat_dpm' => Request()->alamat_dpm,
            'notelp_dpm' => Request()->notelp_dpm,
        ];
        $this->DPMModel->UpdateData($id_dpm, $data);
        return redirect()->route('dpm')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id_dpm)
    {
        $this->DPMModel->DeleteData($id_dpm);
        return redirect()->route('dpm')->with('pesan', 'Data Berhasil ter-delete');
    }
}
