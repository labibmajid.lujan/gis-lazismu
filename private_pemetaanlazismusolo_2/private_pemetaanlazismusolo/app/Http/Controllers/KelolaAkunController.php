<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Password;

class KelolaAkunController extends Controller
{
    public function index()
    {
        $users = User::all();
        return view('akun.index', compact('users'));
    }

    public function add()
    {
        return view('akun.add');
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
        return redirect()->route('kelola-akun')->with('pesan', 'Berhasil tambah data akun!');
    }

    public function edit($id)
    {
        $data_user = User::find($id);
        return view('akun.edit', compact('data_user'));
    }

    public function update($id, Request $request)
    {
        $user = User::find($id);
        $data = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:users,email,' . $user->id,
        ]);

        $user->update($data);
        return redirect()->route('kelola-akun')->with('pesan', 'Berhasil edit data akun!');
    }

    public function update_password($id, Request $request)
    {
        $user = User::find($id);
        $data = $request->validate([
            'password' => ['required', 'confirmed', Password::defaults()],
        ]);

        // dd(Hash::make($data['password']));
        $user->update([
            'password' => Hash::make($data['password'])
        ]);
        return redirect()->route('kelola-akun')->with('pesan', 'Berhasil edit password!');
    }

    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('kelola-akun')->with('pesan', 'Berhasil hapus data akun!');
    }
}
