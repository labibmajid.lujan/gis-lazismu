<?php

namespace App\Http\Controllers;

use App\Models\DPMModel;
use App\Models\PMModel;
use App\Models\ProgramPMModel;
use App\Models\SubprogramModel;
use App\Models\TransaksipmModel;
use Illuminate\Http\Request;

class TransaksiPMController extends Controller
{
    private $TransaksipmModel;
    private $ProgramPMModel;
    private $SubprogramModel;
    private $DPMModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->TransaksipmModel = new TransaksipmModel();
        $this->ProgramPMModel = new ProgramPMModel();
        $this->SubprogramModel = new SubprogramModel();
        $this->DPMModel = new DPMModel();
    }

    public function index()
    {
        $data = [
            'transaksipm' => $this->TransaksipmModel->AllData(),
        ];
        return view('transaksipm.indextpm', $data);
    }

    public function add()
    {
        $data = [
            // 'pm' => $this->PMModel->AllData(),
            'program' => $this->ProgramPMModel->AllData(),
            'subprogram' => $this->SubprogramModel->AllData(),
            'dpm' => $this->DPMModel->AllData(),
        ];
        return view('transaksipm.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'id_dpm' => 'required',
                'id_program' => 'required',
                'id_subprogram' => 'required',
                'tgl_transaksipm' => 'required',
                'ket_transaksipm' => 'required',
            ],
            [
                'id_dpm.required' => 'wajib diisi !!!',
                'id_program.required' => 'wajib diisi !!!',
                'id_subprogram.required' => 'wajib diisi !!!',
                'tgl_transaksipm.required' => 'wajib diisi !!!',
                'ket_transaksipm.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_dpm' => Request()->id_dpm,
            'id_program' => Request()->id_program,
            'id_subprogram' => Request()->id_subprogram,
            'tgl_transaksipm' => Request()->tgl_transaksipm,
            'ket_transaksipm' => Request()->ket_transaksipm,
        ];

        //menyimpan ke modeltransaksi
        $this->TransaksipmModel->InsertData($data);
        return redirect()->route('transaksipm')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_transaksipm)
    {
        $data = [
            // 'pm' => $this->PMModel->AllData(),
            'dpm' => $this->DPMModel->AllData(),
            'program' => $this->ProgramPMModel->AllData(),
            'subprogram' => $this->SubprogramModel->AllData(),
            'transaksipm' => $this->TransaksipmModel->DetailData($id_transaksipm),
        ];
        return view('transaksipm.edit', $data);
    }

    public function update($id_transaksipm)
    {
        request()->validate(
            [
                'id_dpm' => 'required',
                'id_program' => 'required',
                'id_subprogram' => 'required',
                'tgl_transaksipm' => 'required',
                'ket_transaksipm' => 'required',
            ],
            [
                'id_dpm.required' => 'wajib diisi !!!',
                'id_program.required' => 'wajib diisi !!!',
                'id_subprogram.required' => 'wajib diisi !!!',
                'tgl_transaksipm.required' => 'wajib diisi !!!',
                'ket_transaksipm.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_dpm' => Request()->id_dpm,
            'id_program' => Request()->id_program,
            'id_subprogram' => Request()->id_subprogram,
            'tgl_transaksipm' => Request()->tgl_transaksipm,
            'ket_transaksipm' => Request()->ket_transaksipm,
        ];

        $this->TransaksipmModel->UpdateData($id_transaksipm, $data);
        return redirect()->route('transaksipm')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id_transaksipm)
    {
        $this->TransaksipmModel->DeleteData($id_transaksipm);
        return redirect()->route('transaksipm')->with('pesan', 'Data Berhasil ter-delete');
    }

    public function programSearch(Request $request)
    {
        $sub_program = SubprogramModel::where('id_program', $request->id_program)->pluck('nama_subprogram', 'id_subprogram');
        return response()->json([
            'sub_program' => $sub_program
        ]);
    }
}
