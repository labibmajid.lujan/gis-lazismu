<?php

namespace App\Http\Controllers;

use App\Models\ProgramPMModel;
use Illuminate\Http\Request;

class ProgramPMController extends Controller
{
    private $ProgramPMModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->ProgramPMModel = new ProgramPMModel();
    }

    public function index()
    {
        $data = [
            'program' => $this->ProgramPMModel->AllData(),
        ];
        return view('programpm.indexpm', $data);
    }

    public function add()
    {
        $data = [];
        return view('programpm.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'nama_program' => 'required',
                'icon_program' => 'required',
            ],
            [
                'nama_program.required' => 'wajib diisi !!!',
                'icon_program.required' => 'wajib diisi !!!',
            ]
        );

        //
        $file = request()->icon_program; //merequest form dengan nama icon_program
        $filename = $file->getClientOriginalName(); //dengan nama file asli ketika masuk folder
        // $file->move(public_path('iconprogram'), $filename); //kemudian di move dalam folder icon_program
        $file->move(base_path() . '/../public_html/upload/iconprogram', $filename); //kemudian di move dalam folder icon
        
        
        //mengirim variabel data ke JenisModel
        $data = [
            'nama_program' => request()->nama_program,
            'icon_program' => $filename,
        ];
        $this->ProgramPMModel->InsertData($data);
        return redirect()->route('program-pm')->with('pesan', 'Data berhasil di simpan!!!');
    }

    public function edit($id_program)
    {
        $data = [
            'nama_program' => $this->ProgramPMModel->DetailData($id_program),
        ]; //mempassing data ke view edit
        return view('programpm.edit', $data);
    }


    public function update($id_program)
    {
        request()->validate(
            [
                'nama_program' => 'required',
            ],
            [
                'nama_program.required' => 'wajib diisi !!!',
            ]
        );

        if (request()->icon_program <> "") {
            //menghapus icon_program lama di dalam folder setelah diupdate
            $nama_program = $this->ProgramPMModel->DetailData($id_program);
            if ($nama_program->icon_program <> "") {
                // unlink(public_path('iconprogram') . '/' . $nama_program->icon_program);
                unlink(base_path() . '/../public_html/upload/iconprogram' . '/' . $nama_program->icon_program);
                
            }

            //jika ingin ganti icon_program maka 
            $file = request()->icon_program; //merequest form dengan nama icon_program
            $filename = $file->getClientOriginalName(); //dengan nama file asli ketika masuk folder
            // $file->move(public_path('iconprogram'), $filename); //kemudian di move dalam folder iconprogram
            $file->move(base_path() . '/../public_html/upload/iconprogram', $filename);

            //mengirim variabel data ke JenisModel
            $data = [
                'nama_program' => request()->nama_program,
                'icon_program' => $filename,
            ];
            $this->ProgramPMModel->UpdateData($id_program, $data);
        } else {
            //jika tidak ganti icon_program maka
            $data = [
                'nama_program' => request()->nama_program,
            ];
            $this->ProgramPMModel->UpdateData($id_program, $data); //ambil data dari fungsi update
        }
        return redirect()->route('program-pm')->with('pesan', 'Data berhasil di simpan!!!');
    }

    public function delete($id_program)
    {
        //menghapus icon lama
        $nama_program = $this->ProgramPMModel->DetailData($id_program);
        if ($nama_program->icon_program <> "") {
            // unlink(public_path('iconprogram') . '/' . $nama_program->icon_program);
            unlink(base_path() . '/../public_html/upload/iconprogram' . '/' . $nama_program->icon_program);
        }

        $this->ProgramPMModel->DeleteData($id_program);
        return redirect()->route('program-pm')->with('pesan', 'Data Berhasil ter-delete');
    }
}
