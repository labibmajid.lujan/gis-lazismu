<?php

namespace App\Http\Controllers;

use App\Models\DDModel;
use App\Models\DonaturModel;
use App\Models\JenisDModel;
use App\Models\TransaksiModel;
use Illuminate\Http\Request;

class TransaksiController extends Controller
{
    private $TransaksiModel;
    private $DonaturModel;
    private $JenisDModel;
    private $DDModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->TransaksiModel = new TransaksiModel();
        $this->DonaturModel = new DonaturModel();
        $this->JenisDModel = new JenisDModel();
        $this->DDModel = new DDModel();
    }

    public function index()
    {
        $data = [
            'transaksi' => $this->TransaksiModel->AllData(),
        ];
        return view('transaksi.indext', $data);
    }

    public function add()
    {
        $data = [
            'dd' => $this->DDModel->AllData(),
            'donatur' => $this->DonaturModel->AllData(),    //use data donatur
            'jenis' => $this->JenisDModel->AllData(),       //use data jenis donatur
        ];
        return view('transaksi.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'id_dd' => 'required',
                'id_jenisdonatur' => 'required',
                'tgl_transaksi' => 'required',
                'keterangan_transaksi' => 'required',
            ],
            [
                'id_dd.required' => 'wajib diisi !!!',
                'id_jenisdonatur.required' => 'wajib diisi !!!',
                'tgl_transaksi.required' => 'wajib diisi !!!',
                'keterangan_transaksi.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_dd' => Request()->id_dd,
            'id_jenisdonatur' => Request()->id_jenisdonatur,
            'tgl_transaksi' => Request()->tgl_transaksi,
            'keterangan_transaksi' => Request()->keterangan_transaksi,
        ];

        //menyimpan ke modeltransaksi
        $this->TransaksiModel->InsertData($data);
        return redirect()->route('transaksi')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_transaksi)
    {
        $data = [
            'dd' => $this->DDModel->AllData(),
            'donatur' => $this->DonaturModel->AllData(),
            'jenis' => $this->JenisDModel->AllData(),
            'transaksi' => $this->TransaksiModel->DetailData($id_transaksi),
        ];
        return view('transaksi.edit', $data);
    }

    public function update($id_transaksi)
    {
        request()->validate(
            [
                'id_dd' => 'required',
                'id_jenisdonatur' => 'required',
                'tgl_transaksi' => 'required',
                'keterangan_transaksi' => 'required',
            ],
            [
                'id_dd.required' => 'wajib diisi !!!',
                'id_jenisdonatur.required' => 'wajib diisi !!!',
                'tgl_transaksi.required' => 'wajib diisi !!!',
                'keterangan_transaksi.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_dd' => Request()->id_dd,
            'id_jenisdonatur' => Request()->id_jenisdonatur,
            'tgl_transaksi' => Request()->tgl_transaksi,
            'keterangan_transaksi' => Request()->keterangan_transaksi,
        ];

        $this->TransaksiModel->UpdateData($id_transaksi, $data);
        return redirect()->route('transaksi')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id_transaksi)
    {
        $this->TransaksiModel->DeleteData($id_transaksi);
        return redirect()->route('transaksi')->with('pesan', 'Data Berhasil ter-delete');
    }
}
