<?php

namespace App\Http\Controllers;

use App\Models\ProgramPMModel;
use App\Models\SubprogramModel;
use Illuminate\Http\Request;

class SubprogramController extends Controller
{
    private $SubprogramModel;
    private $ProgramPMModel;
    public function __construct()
    {
        $this->middleware('auth');
        $this->SubprogramModel = new SubprogramModel();
        $this->ProgramPMModel = new ProgramPMModel();
    }

    public function index()
    {
        $data = [
            'subprogram' => $this->SubprogramModel->AllData(),
        ];
        return view('subprogrampm.indexsp', $data);
    }

    public function add()
    {
        $data = [
            'program' => $this->ProgramPMModel->AllData(),           //use data program
        ];
        return view('subprogrampm.add', $data);
    }

    public function insert()
    {
        request()->validate(
            [
                'id_program' => 'required',
                'nama_subprogram' => 'required',
            ],
            [
                'id_program.required' => 'wajib diisi !!!',
                'nama_subprogram.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_program' => Request()->id_program,
            'nama_subprogram' => Request()->nama_subprogram,
        ];

        //menyimpan ke modelkecamatan
        $this->SubprogramModel->InsertData($data);
        return redirect()->route('sub-programpm')->with('pesan', 'Data Berhasil Ditambahkan');
    }

    public function edit($id_subprogram)
    {
        $data = [
            'program' => $this->ProgramPMModel->AllData(),
            'subprogram' => $this->SubprogramModel->DetailData($id_subprogram),
        ];
        return view('subprogrampm.edit', $data);
    }

    public function update($id_subprogram)
    {
        request()->validate(
            [
                'id_program' => 'required',
                'nama_subprogram' => 'required',
            ],
            [
                'id_program.required' => 'wajib diisi !!!',
                'nama_subprogram.required' => 'wajib diisi !!!',
            ]
        );

        $data = [
            'id_program' => Request()->id_program,
            'nama_subprogram' => Request()->nama_subprogram,
        ];
        $this->SubprogramModel->UpdateData($id_subprogram, $data);
        return redirect()->route('sub-programpm')->with('pesan', 'Data berhasil Ter-Update');
    }

    public function delete($id_subprogram)
    {
        $this->SubprogramModel->DeleteData($id_subprogram);
        return redirect()->route('sub-programpm')->with('pesan', 'Data Berhasil ter-delete');
    }
}
