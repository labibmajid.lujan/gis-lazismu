@extends('layouts.dashboard-volt')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />


@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-2">
    <div class="d-block mb-4 mb-md-0">
        <p class="mb-0"><small><mark>Tambah data penerima manfaat</mark> jika belum terdapat nama penerima manfaat</mark></small></p>
    </div>
    <div class="d-flex align-items-center">
        <div class="ms-auto d-flex">
            <div class="ms-auto d-flex">
                <a href="/dpm/add" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus-square fa-xs me-2"></i>Tambah Penerima Manfaat</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><b><small><mark style="background-color: orange;">Tambah Data Pemetaan Penerima Manfaat</mark></small></b></h2>
                <form action="/penerima-manfaat/insert" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Nama Penerima Manfaat</small></label>
                                <select class="form-control" id="id_dpm" name="id_dpm">
                                    <option value="">--Pilih Nama--</option>
                                    @foreach ($dpm as $data)
                                    <option value="{{ $data->id_dpm }}">{{ $data->nama_dpm }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('nama_pm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <div>
                                <label for="first_name"><small>Program</small></label>
                                <select class="form-control" id="id_program" name="id_program" onchange="loadData()">
                                    <option value="">--Pilih Program--</option>
                                    @foreach ($program as $data)
                                    <option value="{{ $data->id_program }}">{{ $data->nama_program }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_program')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 mb-3">
                            <div>
                                <label for="first_name"><small>Sub Program</small></label>
                                <select class="form-control" name="id_subprogram" id="id_subprogram">
                                    <option value="">--Pilih Sub Program--</option>

                                </select>
                                <div class="text-danger">
                                    @error('id_subprogram')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Kecamatan</small></label>
                                <select class="form-control" name="id_kecamatan">
                                    <option value="">--Pilih Kecamatan--</option>
                                    @foreach ($kecamatan as $data)
                                    <option value="{{ $data->id_kecamatan }}">{{ $data->kecamatan }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_kecamatan')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="formFile" class="form-label"><small>Foto</small></label>
                            <input class="form-control" type="file" id="formFile" name="foto_pm" accept="image/jpg">
                            <div class="text-danger">
                                @error('foto_pm')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <div>
                                <label for="first_name"><small>Deskripsi</small></label>
                                <textarea class="form-control" name="deskripsi_pm" type="text"></textarea>
                                <div class="text-danger">
                                    @error('deskripsi_pm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Posisi</small></label>
                                <input class="form-control" name="posisi_pm" id="posisi">
                                <div class="text-danger">
                                    @error('posisi')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label><small>Map</small></label>
                            <div id="map" style="width: 100%; height:300px"></div>
                        </div>
                        <div class="row">
                            <div class="mt-3">
                                <a href="/penerima-manfaat" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                                <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection



@push('javascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 12,
        layers: [peta1]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
        // "Biasa": peta4
    };

    L.control.layers(baseMaps).addTo(map);

    //ambil titik kordinat
    var curLocation = [-7.559209, 110.8188121];
    map.attributionControl.setPrefix(false);

    var marker = new L.marker(curLocation, {
        draggable: 'true',
    });
    map.addLayer(marker);

    //// event input kedalam form posisi

    //event perintah fungsi ambil koordinat drag n drop
    marker.on('dragend', function(event) {
        var position = marker.getLatLng();
        marker.setLatLng(position, {
            draggable: 'true',
        }).bindPopup(position).update();
        $("#posisi").val(position.lat + "," + position.lng).keyup();
    });

    //event perintah fungsi ambil koordinat klik
    var posisi = document.querySelector("[name=posisi]");
    map.on("click", function(event) {
        var lat = event.latlng.lat;
        var lng = event.latlng.lng;
        if (!marker) {
            marker = L.marker(event.latlng).addTo(map);
        } else {
            marker.setLatLng(event.latlng);
        }
        posisi.value = lat + "," + lng;
    });

    // select data
    var loadData = function() {
        var id_program = $('#id_program').val();
        $.get('{{ route("searchProgram") }}', {
                id_program: id_program,
                _token: "{{ csrf_token() }}",
            },
            function(data) {
                console.log(data);
                let sub_program = $('#id_subprogram');
                sub_program.empty();
                sub_program.append("<option>" + '--Pilih Sub Program--' + "</option>");
                $.each(data.sub_program, function(key, value) {
                    sub_program.append("<option value='" + key + "'>" + value + "</option>");
                });
            });
    }
</script>

@endpush