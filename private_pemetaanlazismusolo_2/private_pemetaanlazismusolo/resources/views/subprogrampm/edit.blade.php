@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><b><mark style="background-color: orange;">Edit Sub-Program</mark></b></small></h2>
                <form action="/sub-programpm/update/{{$subprogram->id_subprogram}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label><small>Sub Program</small></label>
                                <input class="form-control" value="{{$subprogram->nama_subprogram}}" name="nama_subprogram" type="text">
                                <div class="text-danger">
                                    @error('nama_subprogram')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label><small>Program</small></label>
                            <select class="form-control" name="id_program">
                                <option value="{{$subprogram->id_program}}">{{$subprogram->nama_program}}</option>
                                @foreach ($program as $data)
                                <option value="{{ $data->id_program }}">{{ $data->nama_program }}</option>
                                @endforeach
                            </select>
                            <div class="text-danger">
                                @error('id_program')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/sub-programpm" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection