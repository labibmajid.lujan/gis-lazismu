@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4">Edit Data Jenis Donatur</h2>
                <form action="/program-pm/update/{{$nama_program->id_program}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label>Program</label>
                                <input class="form-control" name="nama_program" type="text" value="{{$nama_program->nama_program}}">
                                <div class="text-danger">
                                    @error('nama_program')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="formFile" class="form-label">Icon</label>
                            <input class="form-control" type="file" id="formFile" name="icon_program" accept="image/png" value="{{$nama_program->icon_program}}">
                            <div class="text-danger">
                                @error('icon_program')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">Save all</button>
                            <a href="/program-pm" type="button" class="btn btn-gray-800 mt-2 animate-up-2">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection