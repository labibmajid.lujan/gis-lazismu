@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Tambah Program</mark></small></h2>
                <form action="/program-pm/insert" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label><small>Program</small></label>
                                <input class="form-control" name="nama_program" type="text">
                                <div class="text-danger">
                                    @error('nama_program')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="formFile" class="form-label"><small>Icon</small></label>
                            <input class="form-control" type="file" id="formFile" name="icon_program" accept="image/png">
                            <div class="text-danger">
                                @error('icon')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/program-pm" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>
@endsection