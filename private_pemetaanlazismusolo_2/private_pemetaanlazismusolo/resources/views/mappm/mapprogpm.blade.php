@extends('layouts.dashboard-volt')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />

<style>
    #map {
        height: 400px;
    }
</style>
@endsection

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-4">
    <div class="d-block mb-4 mb-md-0">
        <h2 class="h4"><mark style="background-color: orange">Map Penerima Manfaat</mark></h2>
        <p class="mb-0"><small>Kelola data penerima manfaat berdasarkan pemetaan per-kecamatan & program. <br>
                | <mark style="background-color: orange"><a href="/penerima-manfaat/add"> + <i>click</i> untuk tambah data pemetaan penerima manfaat</a></mark></small></p>
    </div>
    <div class="btn-toolbar mb-2 mb-md-4">
        <div class="dropdown">
            <button class="btn btn-gray-800 d-inline-flex align-items-center dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-chevron-circle-down fa-xs me-2"></i>
                Kecamatan
            </button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-start mt-2 py-1">
                @foreach (@$kecamatan as $data)
                <li><a class="dropdown-item" href="/mapkecamatanpm/{{ $data->id_kecamatan }}">{{$data->kecamatan}}</a></li>
                @endforeach
            </div>
        </div>
        <div class="dropdown ms-2 ms-lg-3">
            <button class="btn btn-gray-800 d-inline-flex align-items-center dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-chevron-circle-down fa-xs me-2"></i>
                Program
            </button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-start mt-2 py-1">
                @foreach ($program as $data)
                <li><a class="dropdown-item" href="/mapprogrampm/{{ $data->id_program }}">{{$data->nama_program}}</a></li>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b><small>Map Penerima Manfaat Program {{$prog->nama_program}}</small></b></div>
                <div class="card-body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="py-4"></div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b><small>Data Penerima Manfaat Program {{$prog->nama_program}}</small></b></div>
                <div class="card-body">
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th width="40px" class="text-center">No</th>
                                <th width="40px" class="text-center">Nama</th>
                                <th>Program</th>
                                <th>Alamat</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach ($pm as $data)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td>{{ $data->nama_dpm }}</td>
                                <td>{{ $data->nama_program }}</td>
                                <td>{{ $data->alamat_dpm }}</td>
                                <th>{{ $data->keterangan_dpm }}</th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('javascript')
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });


    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    // var peta4 = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    //     maxZoom: 19,
    //     attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    // });


    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 12,
        layers: [peta2]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
        // "Biasa": peta4
    };

    //menampilkan tombol checbox kecamatan, donatur pada layer

    L.control.layers(baseMaps).addTo(map);


    //menampilkan batas kecamatan dengan mengambil data dari fungsi donatur_map pada HomeController
    @foreach($kecamatan as $data)
    L.geoJSON(<?= $data->geojson ?>, {
        style: {
            color: 'white', //color samping
            fillColor: '{{$data->warna}}', //color isi
            fillOpacity: 0.1,
        }
    }).addTo(map).bindPopup("{{$data->kecamatan}}");
    @endforeach

    //perulangan marker data donatur
    var ProgramIcon = L.Icon.extend({
        options: {
            iconSize: [40, 40],
        }
    });

    @foreach($pm as $data)
    //menampilkan popup informasi
    var infopopup = '<table class="table table-bordered"><tr><th><b>Nama :</b></th><th>{{ $data->nama_dpm}}</th></tr><tr><td><b>Program :</b></td><td>{{ $data->nama_program}}</td></tr><tr><td><b>Alamat :</b></td><td>{{ $data->alamat_dpm}}</td></tr><tr><td colspan="2" class="text-center"><a href="/detaildataPM/{{$data->id_pm}}" class="btn btn-sm btn-warning">Detail Data</a></td></tr></table>'

    L.marker([<?= $data->posisi_pm ?>], {
        icon: new ProgramIcon({
            iconUrl: "{{ asset('iconprogram') }}/{{ $data->icon_program}}" //sesuai dengan marker icon jenis
        })
    }).addTo(map).bindPopup(infopopup);
    @endforeach
</script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>

@endpush