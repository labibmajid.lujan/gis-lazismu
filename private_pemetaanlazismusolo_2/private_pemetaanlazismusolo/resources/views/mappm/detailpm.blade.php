@extends('layouts.dashboard-volt')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />


@section('content')
<div class="ms-auto d-flex mb-2">
    <a href="/pm-map" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fas fa-angle-left me-2"></i>kembali</a>
</div>
<div class="col-md-6 mb-3">
    <div class="card">
        <div class="card border-0 shadow">
            <div class="card-header"><small><mark style="background-color: orange"><b>Map</mark> Penerima Manfaat</b></small></div>
            <div class="card-body">
                <div id="map" style="width: 100%; height:430px"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <!--  -->
    <div class="card">
        <div class="card border-0 shadow">
            <div class="card-header"><small><b>Foto Penerima Manfaat</b></small></div>
            <div class="card-body">
                <img src="{{ url('/') . '/upload/fotopm/' . $pm->foto_pm }}" class="img-fluid col-12">
            </div>
        </div>
    </div>
</div>
<div class="py-2"></div>
<div class="col-sm-12">
    <table class="table table-bordered">
        <tr>
            <td width="200px">Penerima Manfaat</td>
            <td class="text-center" width="30px">:</td>
            <td> {{$pm->nama_dpm}} </td>
        </tr>
        <tr>
            <td>Program</td>
            <td class="text-center">:</td>
            <td> {{$pm->nama_program}} </td>
        </tr>
        <tr>
            <td>Sub Program</td>
            <td class="text-center">:</td>
            <td> {{$pm->nama_subprogram}} </td>
        </tr>
        <tr>
            <td>Kecamatan</td>
            <td class="text-center">:</td>
            <td> {{$pm->kecamatan}} </td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td class="text-center">:</td>
            <td> {{$pm->alamat_dpm}} </td>
        </tr>
        <tr>
            <td>Keterangan</td>
            <td class="text-center">:</td>
            <td> {{$pm->deskripsi_pm}} </td>
        </tr>
    </table>
</div>
@endsection


@push('javascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var map = L.map('map', {
        center: [<?= $pm->posisi_pm ?>],
        zoom: 18,
        layers: [peta2]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
        // "Biasa": peta4
    };

    L.control.layers(baseMaps).addTo(map);

    var DonaturIcon = L.Icon.extend({
        options: {
            iconSize: [40, 40],
        }
    });

    L.marker([<?= $pm->posisi_pm ?>], {
        icon: new DonaturIcon({
            iconUrl: "{{ asset('iconprogram') }}/{{ $pm->icon_program}}" //sesuai dengan marker icon jenis
        })
    }).addTo(map);
</script>
@endpush