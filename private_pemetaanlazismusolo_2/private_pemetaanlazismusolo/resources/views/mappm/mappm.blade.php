@extends('layouts.dashboard-volt')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />

<style>
    #map {
        height: 700px;
    }
</style>
@endsection

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-4">
    <div class="d-block mb-4 mb-md-0">
        <h2 class="h4"><mark style="background-color: orange">Map Penerima Manfaat</mark></h2>
        <p class="mb-0"><small>Kelola data penerima manfaat berdasarkan pemetaan per-kecamatan & program. <br>
                | <mark style="background-color: orange"><a href="/penerima-manfaat/add"> + <i>click</i> untuk tambah data pemetaan penerima manfaat</a></mark></small></p>
    </div>
    <div class="btn-toolbar mb-2 mb-md-4">
        <div class="dropdown">
            <button class="btn btn-gray-800 d-inline-flex align-items-center dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="top" title="pilih, untuk melihat pemetaan per-kecamatan">
                <i class="fa fa-chevron-circle-down fa-xs me-2"></i>
                Kecamatan
            </button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-start mt-2 py-1">
                @foreach (@$kecamatan as $data)
                <li><a class="dropdown-item" href="/mapkecamatanpm/{{ $data->id_kecamatan }}">{{$data->kecamatan}}</a></li>
                @endforeach
            </div>
        </div>
        <div class="dropdown ms-2 ms-lg-3">
            <button class="btn btn-gray-800 d-inline-flex align-items-center dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle="tooltip" data-placement="top" title="pilih, untuk melihat pemetaan per-program">
                <i class="fa fa-chevron-circle-down fa-xs me-2"></i>
                Program
            </button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-start mt-2 py-1">
                @foreach ($program as $data)
                <li><a class="dropdown-item" href="/mapprogrampm/{{ $data->id_program }}">{{$data->nama_program}}</a></li>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });


    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var kecamatan = L.layerGroup(); //menambahkan layer kecamatan
    var pm = L.layerGroup(); //menambahkan layer donatur

    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 13,
        layers: [peta2, kecamatan, pm]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
    };

    //menampilkan tombol checbox kecamatan, donatur pada layer
    var overlayer = {
        "Kecamatan": kecamatan,
        "Penerima-Manfaat": pm,
    };

    L.control.layers(baseMaps, overlayer).addTo(map);


    //menampilkan batas kecamatan dengan mengambil data dari fungsi donatur_map pada HomeController
    @foreach($kecamatan as $data)
    L.geoJSON(<?= $data->geojson ?>, {
        style: {
            color: 'white', //color samping
            fillColor: '{{$data->warna}}', //color isi
            fillOpacity: 0.1,
        }
    }).addTo(kecamatan).bindPopup("{{$data->kecamatan}}");
    @endforeach

    var ProgramIcon = L.Icon.extend({
        options: {
            iconSize: [25, 25],
        }
    });

    @foreach($pm as $data)
    //menampilkan popup informasi
    var infopopup = '<div class="text-center"><mark style="background-color: orange">Nama: {{ $data->nama_dpm}}</mark><br> Alamat: {{ $data->alamat_dpm}}<br> <a href="detaildataPM/{{$data->id_pm}}" class="btn btn-xs btn-warning">Detail Data</a></div>'

    L.marker([<?= $data->posisi_pm ?>], {
        icon: new ProgramIcon({
            iconUrl: "{{ asset('iconprogram') }}/{{ $data->icon_program}}" //sesuai dengan marker icon jenis
        })
    }).addTo(pm).bindPopup(infopopup);
    @endforeach
</script>


@endpush