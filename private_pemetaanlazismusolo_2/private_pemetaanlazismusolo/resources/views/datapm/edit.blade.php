@extends('layouts.dashboard-volt')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><b><small><mark style="background-color: orange;">Edit Data Penerima Manfaat</mark></small></b></h2>
                <form action="/dpm/update/{{$dpm->id_dpm}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Nama Donatur</small></label>
                                <input class="form-control" value="{{$dpm->nama_dpm}}" name="nama_dpm" id="first_name" type="text">
                                <div class="text-danger">
                                    @error('nama_dpm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Kecamatan</small></label>
                                <select class="form-control" name="id_kecamatan">
                                    <option value="{{$dpm->id_kecamatan}}">{{$dpm->kecamatan}}</option>
                                    @foreach ($kecamatan as $data)
                                    <option value="{{ $data->id_kecamatan }}">{{ $data->kecamatan }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_kecamatan')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Alamat</small></label>
                                <textarea class="form-control" name="alamat_dpm" type="text">{{$dpm->alamat_dpm}}</textarea>
                                <div class="text-danger">
                                    @error('alamat_dpm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>No Telpon</small></label>
                                <input class="form-control" value="{{$dpm->notelp_dpm}}" name="notelp_dpm" id="notelp_dpm">
                                <div class="text-danger">
                                    @error('notelp_dpm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/dpm" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')

@endpush