@extends('layouts.dashboard-volt')

@section('css')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Edit Data Transaksi</mark></small></h2>
                <form action="/transaksi/update/{{$transaksi->id_transaksi}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Donatur</small></label>
                                <select class="form-control" name="id_dd">
                                    <option value="{{ $transaksi->id_dd }}"> {{ $transaksi->nama_dd }}</option>
                                    @foreach ($dd as $data)
                                    <option value="{{ $data->id_dd }}">{{ $data->nama_dd }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_donatur')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Jenis Donatur</small></label>
                                <select class="form-control" name="id_jenisdonatur">
                                    <option value="{{ $transaksi->id_jenisdonatur }}"> {{ $transaksi->jenis_donatur }}</option>
                                    @foreach ($jenis as $data)
                                    <option value="{{ $data->id_jenisdonatur }}">{{ $data->jenis_donatur }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_jenisdonatur')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Tanggal</small></label>
                                <input class="form-control" value="{{$transaksi->tgl_transaksi}}" name="tgl_transaksi" id="first_name" type="date">
                                <div class="text-danger">
                                    @error('tgl_transaksi')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 mb-3">
                            <div>
                                <label for="first_name"><small>Keterangan</small></label>
                                <textarea class="form-control" name="keterangan_transaksi" type="text">{{$transaksi->keterangan_transaksi}}</textarea>
                                <div class="text-danger">
                                    @error('keterangan_transaksi')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mt-3">
                                <a href="/transaksi" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                                <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection



@push('javascript')

@endpush