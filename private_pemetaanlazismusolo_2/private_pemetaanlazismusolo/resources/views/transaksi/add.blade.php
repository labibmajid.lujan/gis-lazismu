@extends('layouts.dashboard-volt')

@section('css')

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-2">
    <div class="d-block mb-4 mb-md-0">
        <p class="mb-0"><small><mark>Tambah data donatur</mark> jika belum terdapat nama Donatur</small></p>
    </div>
    <div class="d-flex align-items-center">
        <div class="ms-auto d-flex">
            <div class="ms-auto d-flex">
                <a href="/dd/add" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus-square fa-xs me-2"></i>Tambah Data Donatur</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Tambah Data Transaksi</mark></small></h2>
                <form action="/transaksi/insert" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Nama Donatur</small></label>
                                <select class="form-control" name="id_dd">
                                    <option value="">--Pilih Nama Donatur--</option>
                                    @foreach ($dd as $data)
                                    <option value="{{ $data->id_dd }}">{{ $data->nama_dd }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_dd')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Jenis Donatur</small></label>
                                <select class="form-control" name="id_jenisdonatur">
                                    <option value="">--Pilih Jenis Donatur--</option>
                                    @foreach ($jenis as $data)
                                    <option value="{{ $data->id_jenisdonatur }}">{{ $data->jenis_donatur }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_jenisdonatur')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Tanggal</small></label>
                                <input class="form-control" name="tgl_transaksi" id="first_name" type="date">
                                <div class="text-danger">
                                    @error('tgl_transaksi')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 mb-3">
                            <div>
                                <label for="first_name"><small>Keterangan</small></label>
                                <textarea class="form-control" name="keterangan_transaksi" type="text"></textarea>
                                <div class="text-danger">
                                    @error('keterangan_transaksi')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mt-3">
                                <a href="/transaksi" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                                <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection



@push('javascript')

@endpush