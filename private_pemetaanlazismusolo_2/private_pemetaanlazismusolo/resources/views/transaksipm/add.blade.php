@extends('layouts.dashboard-volt')

@section('css')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><b><small><mark style="background-color: orange;">Tambah Data Transaksi</mark></small></b></h2>
                <form action="/transaksipm/insert" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Nama Penerima Manfaat</small></label>
                                <select class="form-control" name="id_dpm">
                                    <option value="">--Pilih Nama--</option>
                                    @foreach ($dpm as $data)
                                    <option value="{{ $data->id_dpm }}">{{ $data->nama_dpm }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_dpm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Program</small></label>
                                <select class="form-control" id="id_program" name="id_program" onchange="loadData()">
                                    <option value="">--Pilih Program--</option>
                                    @foreach ($program as $data)
                                    <option value="{{ $data->id_program }}">{{ $data->nama_program }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_program')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>SubProgram</small></label>
                                <select class="form-control" name="id_subprogram" id="id_subprogram">
                                    <option value="">--Pilih Sub Program--</option>
                                    @foreach ($subprogram as $data)
                                    <option value="{{ $data->id_subprogram }}">{{ $data->nama_subprogram }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_subprogram')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 mb-3">
                            <div>
                                <label for="first_name"><small>Tanggal</small></label>
                                <input class="form-control" name="tgl_transaksipm" type="date"></input>
                                <div class="text-danger">
                                    @error('tgl_transaksipm')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 mb-3">
                                <div>
                                    <label for="first_name"><small>Keterangan</small></label>
                                    <textarea class="form-control" name="ket_transaksipm" type="text"></textarea>
                                    <div class="text-danger">
                                        @error('ket_transaksipm')
                                        {{$message}}
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="mt-3">
                                <a href="/transaksipm" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                                <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection



@push('javascript')
<script>
    // select data
    var loadData = function() {
        var id_program = $('#id_program').val();
        $.get('{{ route("searchProgram") }}', {
                id_program: id_program,
                _token: "{{ csrf_token() }}",
            },
            function(data) {
                console.log(data);
                let sub_program = $('#id_subprogram');
                sub_program.empty();
                sub_program.append("<option>" + '--Pilih Sub Program--' + "</option>");
                $.each(data.sub_program, function(key, value) {
                    sub_program.append("<option value='" + key + "'>" + value + "</option>");
                });
            });
    }
</script>

@endpush