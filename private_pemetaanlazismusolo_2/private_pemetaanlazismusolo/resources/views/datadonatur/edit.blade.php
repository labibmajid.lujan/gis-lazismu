@extends('layouts.dashboard-volt')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Edit Data Donatur</mark></small></h2>
                <form action="/dd/update/{{$dd->id_dd}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Nama Donatur</small></label>
                                <input class="form-control" value="{{$dd->nama_dd}}" name="nama_dd" id="first_name" type="text">
                                <div class="text-danger">
                                    @error('nama_dd')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Kecamatan</small></label>
                                <select class="form-control" name="id_kecamatan">
                                    <option value="{{$dd->id_kecamatan}}">{{$dd->kecamatan}}</option>
                                    @foreach ($kecamatan as $data)
                                    <option value="{{ $data->id_kecamatan }}">{{ $data->kecamatan }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_kecamatan')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Alamat</small></label>
                                <textarea class="form-control" name="alamat_dd" type="text">{{$dd->alamat_dd}}</textarea>
                                <div class="text-danger">
                                    @error('alamat_dd')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Telpon</small></label>
                                <input class="form-control" value="{{$dd->notelp_dd}}" name="notelp_dd" id="notelp_dd">
                                <div class="text-danger">
                                    @error('notelp_dd')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/dd" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('javascript')

@endpush