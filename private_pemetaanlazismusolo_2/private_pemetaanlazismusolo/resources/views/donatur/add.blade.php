@extends('layouts.dashboard-volt')
@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />


@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-2">
    <div class="d-block mb-4 mb-md-0">
        <p class="mb-0"><small><mark>Tambah data donatur</mark> jika belum terdapat nama Donatur</mark></small></p>
    </div>
    <div class="d-flex align-items-center">
        <div class="ms-auto d-flex">
            <div class="ms-auto d-flex">
                <a href="/dd/add" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-plus-square fa-xs me-2"></i>Tambah Data Donatur</a>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Tambah Data Pemetaan Donatur</mark></small></h2>
                <form action="/donatur/insert" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Nama Donatur</small></label>
                                <select class="form-control" name="id_dd">
                                    <option value="">--Pilih Nama Donatur--</option>
                                    @foreach ($dd as $data)
                                    <option value="{{ $data->id_dd }}">{{ $data->nama_dd }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_dd')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Jenis Donatur</small></label>
                                <select class="form-control" name="id_jenisdonatur">
                                    <option value="">--Pilih Jenis Donatur--</option>
                                    @foreach ($jenis as $data)
                                    <option value="{{ $data->id_jenisdonatur }}">{{ $data->jenis_donatur }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_jenisdonatur')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Kecamatan</small></label>
                                <select class="form-control" name="id_kecamatan">
                                    <option value="">--Pilih Kecamatan--</option>
                                    @foreach ($kecamatan as $data)
                                    <option value="{{ $data->id_kecamatan }}">{{ $data->kecamatan }}</option>
                                    @endforeach
                                </select>
                                <div class="text-danger">
                                    @error('id_kecamatan')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Posisi</small></label>
                                <input class="form-control" name="posisi" id="posisi">
                                <div class="text-danger">
                                    @error('posisi')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label>Map</label>
                            <div id="map" style="width: 100%; height:300px"></div>
                        </div>
                        <div class="row">
                            <div class="mt-3">
                                <a href="/donatur" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                                <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection



@push('javascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 12,
        layers: [peta1]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
        // "Biasa": peta4
    };

    L.control.layers(baseMaps).addTo(map);

    //ambil titik kordinat
    var curLocation = [-7.559209, 110.8188121];
    map.attributionControl.setPrefix(false);

    var marker = new L.marker(curLocation, {
        draggable: 'true',
    });
    map.addLayer(marker);

    //// event input kedalam form posisi

    //event perintah fungsi ambil koordinat drag n drop
    marker.on('dragend', function(event) {
        var position = marker.getLatLng();
        marker.setLatLng(position, {
            draggable: 'true',
        }).bindPopup(position).update();
        $("#posisi").val(position.lat + "," + position.lng).keyup();
    });

    //event perintah fungsi ambil koordinat klik
    var posisi = document.querySelector("[name=posisi]");
    map.on("click", function(event) {
        var lat = event.latlng.lat;
        var lng = event.latlng.lng;
        if (!marker) {
            marker = L.marker(event.latlng).addTo(map);
        } else {
            marker.setLatLng(event.latlng);
        }
        posisi.value = lat + "," + lng;
    });
</script>

@endpush