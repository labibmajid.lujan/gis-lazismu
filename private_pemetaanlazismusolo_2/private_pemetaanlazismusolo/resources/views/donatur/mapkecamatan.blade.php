@extends('layouts.dashboard-volt')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />

<style>
    #map {
        height: 400px;
    }
</style>
@endsection

@section('content')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-2">
    <div class="d-block mb-4 mb-md-0">
        <h2 class="h4"><mark style="background-color: orange">Map Donatur</mark></h2>
        <p class="mb-0"><small>Kelola data donatur berdasarkan pemetaan per-kecamatan dan jenis donatur. | <mark style="background-color: orange"><a href="/donatur"> + <i>click</i> untuk kelola data pemetaan</a></mark></small></p>
    </div>
    <div class="btn-toolbar mb-2 mb-md-4">
        <div class="dropdown">
            <button class="btn btn-gray-800 d-inline-flex align-items-center dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-chevron-circle-down fa-xs me-2"></i>
                Kecamatan
            </button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-start mt-2 py-1">
                @foreach (@$kecamatan as $data)
                <li><a class="dropdown-item" href="/mapkecamatan/{{ $data->id_kecamatan }}">{{$data->kecamatan}}</a></li>
                @endforeach
            </div>
        </div>
        <div class="dropdown ms-2 ms-lg-3">
            <button class="btn btn-gray-800 d-inline-flex align-items-center dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-chevron-circle-down fa-xs me-2"></i>
                Jenis
            </button>
            <div class="dropdown-menu dashboard-dropdown dropdown-menu-start mt-2 py-1">
                @foreach (@$jenis as $data)
                <li><a class="dropdown-item" href="/mapjenis/{{ $data->id_jenisdonatur }}">{{$data->jenis_donatur}}</a></li>
                @endforeach
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b><small> Map Kecamatan {{$kec->kecamatan}}</small></b></div>
                <div class="card-body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="py-4"></div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><b><small>Data Donatur {{$kec->kecamatan}}</small></b></div>
                <div class="card-body">
                    <table id="example" class="table table-striped" style="width:100%">
                        <thead>
                            <tr>
                                <th width="40px" class="text-center">No</th>
                                <th width="40px" class="text-center">Nama Donatur</th>
                                <th>Alamat</th>
                                <th>Jenis</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $no = 1; ?>
                            @foreach ($donatur as $data)
                            <tr>
                                <td class="text-center">{{ $no++ }}</td>
                                <td>{{ $data->nama_dd }}</td>
                                <td>{{ $data->alamat_dd }}</td>
                                <td>{{ $data->jenis_donatur }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('javascript')
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var kecamatan = L.layerGroup();
    var donatur = L.layerGroup(); //menambahkan layer donatur

    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 13,
        layers: [peta2, kecamatan, donatur]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
    };

    //menampilkan tombol checbox kecamatan pada layer
    var overlayer = {
        "Kecamatan": kecamatan,
        "Donatur": donatur,
    };

    L.control.layers(baseMaps, overlayer).addTo(map);

    //menampilkan batas kecamatan dengan mengambil data dari fungsi donatur_map pada HomeController
    var kec = L.geoJSON(<?= $kec->geojson ?>, { //memanggil data perkecamatan
        style: {
            color: 'white', //color samping
            fillColor: '{{$kec->warna}}', //color isi
            fillOpacity: 0.1,
        }
    }).addTo(kecamatan).bindPopup("{{$kec->kecamatan}}");

    map.fitBounds(kec.getBounds()); //fokus pada kecamatan yg dipanggil

    //perulangan marker data donatur
    var DonaturIcon = L.Icon.extend({
        options: {
            iconSize: [40, 40],
        }
    });

    @foreach($donatur as $data)
    //menampilkan popup informasi
    var infopopup = '<table class="table table-bordered"><tr><th>{{ $data->nama_dd}}</th></tr><td>{{ $data->alamat_dd}}</td></tr></table>';

    L.marker([<?= $data->posisi ?>], {
        icon: new DonaturIcon({
            iconUrl: "{{ asset('icon') }}/{{ $data->icon}}" //sesuai dengan marker icon jenis
        })
    }).addTo(donatur).bindPopup(infopopup);
    @endforeach
</script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>


@endpush