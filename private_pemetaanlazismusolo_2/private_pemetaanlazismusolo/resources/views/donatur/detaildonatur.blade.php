@extends('layouts.dashboard-volt')

@section('css')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.3/dist/leaflet.css" integrity="sha256-kLaT2GOSpHechhsozzB+flnD+zUyjE2LlfWPgU04xyI=" crossorigin="" />


@section('content')
<div class="ms-auto d-flex mb-2">
    <a href="/donatur-map" type="button" class="btn btn-sm btn-primary btn-flat"><i class="fas fa-angle-left me-2"></i>kembali</a>
</div>
<div class="col-md-6 mb-3">
    <div class="card border-0 shadow">
        <div class="card-header"><small><mark style="background-color: orange">Map</mark>Donatur</small></div>
        <div class="card-body">
            <div id="map" style="width: 100%; height:400px"></div>
        </div>
    </div>
</div>

<div class="col-md-6">
    <div class="card mb-2">
        <div class="card border-0 shadow">
            <div class="card-header"><small>Jumlah Data Transaksi {{$data->nama_dd}}</small></div>
            <div class="card-body">
                <div class="row d-block d-xl-flex align-items-center">
                    <div class="col-12 col-xl-5 text-xl-center mb-3 mb-xl-0 d-flex align-items-center justify-content-xl-center">
                        <div class="icon-shape icon-shape-primary rounded me-4 me-sm-0">
                            <svg class="icon" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z">
                                </path>
                            </svg>
                        </div>
                        <div class="d-sm-none">
                            <h2 class="fw-extrabold h5">Data Transaksi</h2>
                            <h3 class="mb-1">-</h3>
                        </div>
                    </div>
                    <div class="col-12 col-xl-7 px-xl-0">
                        <div class="d-none d-sm-block">
                            <h2 class="h6 text-gray-400 mb-0">Data Transaksi</h2>
                            <h3 class="fw-extrabold mb-2">-</h3>
                        </div>
                        <div class="small d-flex mt-1">
                            <a href="/rekaptransaksi/{{$data->id_dd}}">Selengkapnya <svg class="icon icon-xs text-danger" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd"></path></a>
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card border-0 shadow">
            <div class="card-header"><small>Foto Penerima Manfaat</small></div>
            <div class="card-body">
                <table class="table table-bordered">
                    <tr>
                        <td width="50px">Donatur</td>
                        <td class="text-center" width="20px">:</td>
                        <td> {{$data->nama_dd}} </td>
                    </tr>
                    <tr>
                        <td>Jenis</td>
                        <td class="text-center">:</td>
                        <td> {{ $data->data_pemetaan }} </td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td class="text-center">:</td>
                        <td> {{$data->id_kecamatan}} </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection


@push('javascript')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script src="https://unpkg.com/leaflet@1.9.3/dist/leaflet.js" integrity="sha256-WBkoXOwTeyKclOHuWtc+i2uENFpDZ9YPdf5Hf+D7ewM=" crossorigin=""></script>
<script>
    var peta1 = L.tileLayer('http://{s}.google.com/vt?lyrs=p&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta2 = L.tileLayer('http://{s}.google.com/vt?lyrs=s&x={x}&y={y}&z={z}', {
        maxZoom: 20,
        subdomains: ['mt0', 'mt1', 'mt2', 'mt3']
    });

    var peta3 = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    });

    var map = L.map('map', {
        center: [-7.559209, 110.8188121],
        zoom: 12,
        layers: [peta1]
    });

    var baseMaps = {
        "Terain": peta1,
        "Satelite": peta2,
        "Streets": peta3
        // "Biasa": peta4
    };

    L.control.layers(baseMaps).addTo(map);

    var DonaturIcon = L.Icon.extend({
        options: {
            iconSize: [40, 40],
        }
    });

    L.marker([<?= $data->posisi ?>], {
        icon: new DonaturIcon({
            iconUrl: "{{ asset('icon') }}/{{ $data->icon}}" //sesuai dengan marker icon jenis
        })
    }).addTo(map);
</script>
@endpush