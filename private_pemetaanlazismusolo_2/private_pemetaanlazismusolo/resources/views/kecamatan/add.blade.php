@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><b><mark style="background-color: orange;">Tambah Data</mark></b></small></h2>
                <form action="/kecamatan/insert" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label for="first_name"><small>Kecamatan</small></label>
                                <input class="form-control" name="kecamatan" id="first_name" type="text">
                                <div class="text-danger">
                                    @error('kecamatan')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="exampleColorInput" class="form-label"><small>Warna</small></label>
                            <input type="color" name="warna" class="form-control form-control-color" id="exampleColorInput" value="#563d7c" title="Choose your color">
                            <!-- <div class="input-group my-colorpicker2">
                                    <input class="form-control" id="my-colorpicker2" type="text" required>

                                    <div class="input-group-append animate-up-2">
                                        <span class="input-group-text animate-up-2"><i class="fas fa-square"></i></span>
                                    </div>
                                </div> -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 mb-3">
                            <div class="form-group">
                                <label for="address"><small>GeoJson</small></label>
                                <textarea name="geojson" id="example" class="form-control" rows="4"></textarea>
                            </div>
                        </div>
                        <div class="text-danger">
                            @error('geojson')
                            {{$message}}
                            @enderror
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/kecamatan" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
            </div>
        </div>
        </form>
    </div>
</div>
</div>
</div>
@endsection