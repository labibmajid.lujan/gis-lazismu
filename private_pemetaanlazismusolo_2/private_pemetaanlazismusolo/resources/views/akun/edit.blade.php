@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Edit Data Akun</mark></small></h2>
                <form action="{{ route('akun-update', $data_user->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label><small>Nama</small></label>
                                <input class="form-control" name="name" type="text" value="{{old('name', $data_user->name) }}">
                                <div class="text-danger">
                                    @error('name')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="formFile" class="form-label"><small>Email</small></label>
                            <input class="form-control" type="email" name="email" value="{{ old('email', $data_user->email) }}">
                            <div class="text-danger">
                                @error('email')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/kelola-akun" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;">Edit Password</mark></small></h2>
                <form action="{{ route('akun-update-password', $data_user->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label><small>Password</small></label>
                                <input class="form-control" name="password" type="password" value="{{old('password') }}">
                                <div class="text-danger">
                                    @error('password')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="formFile" class="form-label"><small>Password confirmation</small></label>
                            <input class="form-control" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}">
                            <div class="text-danger">
                                @error('email')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/kelola-akun" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection