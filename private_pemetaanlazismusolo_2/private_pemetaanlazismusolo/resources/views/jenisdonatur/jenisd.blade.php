@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h6><b>Jenis Donatur</b></h6>
                        <div class="ms-auto d-flex">
                            <a href="/jenis-donatur/add" type="button" class="btn btn-sm btn-primary btn-flat" data-toggle="tooltip" data-placement="top" title="tambah jenis donatur"><i class="fas fa-plus fa-xs me-2"></i>tambah</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if (session('pesan'))
                    <div class="alert alert-primary d-flex align-items-center" role="alert">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-exclamation-triangle-fill flex-shrink-0 me-2" viewBox="0 0 16 16" role="img" aria-label="Warning:">
                            <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z" />
                        </svg>
                        <div>
                            {{session('pesan')}}
                        </div>
                    </div>
                    @endif
                    <div class="table-responsive">
                        <table id="example" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th width="40px" class="text-center">No</th>
                                    <th>Name</th>
                                    <th width="40px" class="text-center">Icon</th>
                                    <th width="40px" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($jenis as $data)
                                <tr>
                                    <td class="text-center">{{ $no++ }}</td>
                                    <td>{{ $data->jenis_donatur}}</td>
                                    <td><img src="{{ url('/') . '/upload/icon/' . $data->icon}}" width="60px"></td>
                                    <td>
                                        <a href="/jenis-donatur/edit/ {{$data -> id_jenisdonatur}}" class="btn btn-sm btn-flatt btn-warning" data-toggle="tooltip" data-placement="top" title="edit data"><i class="far fa-edit"></i></a>
                                        <button class="btn btn-sm btn-flatt btn-warning" data-bs-toggle="modal" data-bs-target="#delete{{$data -> id_jenisdonatur}}" data-toggle="tooltip" data-placement="top" title=" hapus data"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                        <div class="modal fade" id="delete{{ $data->id_jenisdonatur }}" tabindex="-1" role="dialog" aria-labelledby="modal-default" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h2 class="h6 modal-title">Terms of Service</h2>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Apakah anda akan menghapus data ini?</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <a href="/jenis-donatur/delete/{{$data -> id_jenisdonatur}}" type="button" class="btn btn-secondary">Accept</a>
                                                        <button type="button" class="btn btn-link text-gray-600 ms-auto" data-bs-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


@push('javascript')

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    });
</script>
@endpush