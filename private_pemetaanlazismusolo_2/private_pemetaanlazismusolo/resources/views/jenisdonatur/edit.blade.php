@extends('layouts.dashboard-volt')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card card-body border-0 shadow mb-4">
                <h2 class="h5 mb-4"><small><mark style="background-color: orange;"> Data Jenis Donatur</mark></small></h2>
                <!-- {{ $jenis_donatur->id_jenisdonatur }} menerima passing dari fungsi edit -->
                <form action="/jenis-donatur/update/{{$jenis_donatur->id_jenisdonatur}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <div>
                                <label><small>Jenis Donatur</small></label>
                                <input class="form-control" name="jenis_donatur" type="text" value="{{$jenis_donatur->jenis_donatur}}">
                                <div class="text-danger">
                                    @error('jenis_donatur')
                                    {{$message}}
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="formFile" class="form-label"><small>Icon</small></label>
                            <input class="form-control" type="file" id="formFile" name="icon" accept="image/png" value="{{$jenis_donatur->icon}}">
                            <div class="text-danger">
                                @error('icon')
                                {{$message}}
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mt-3">
                            <a href="/jenis-donatur" type="button" class="btn btn-gray-800 mt-2 animate-up-2">kembali</a>
                            <button class="btn btn-gray-800 mt-2 animate-up-2" type="submit">simpan</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection